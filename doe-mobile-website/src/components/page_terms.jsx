import React, { Component } from "react";
import Common_header from "./common_header";
import { Link } from 'react-router-dom';
class TermsnCondition extends Component {
    render(){
      const content = this.props.content;
      console.log(content);
      return(
            <div className="privacy_terms__page">
                <Common_header/>
                <div className="privacy_terms_title">
                    <img src="./../assets/terms.png" />
                    <h2>Terms of services</h2>
                </div>
                {
                  content.TERMS_OF_USE.map( (data,i) => <div key={i} className="privacy_terms_content">
                      <h4 dangerouslySetInnerHTML={{ __html: data.summary }}></h4>
                      {
                        data.points.map( (point,j) => <p dangerouslySetInnerHTML={{ __html: point }} key={j}></p>)
                      }
                      <p>For information about how to contact DOE CARDS SOLUTIONS PVT. LTD, please visit our <Link to="/joinus">Join Us</Link>page.</p>
                  </div>)
                }

            </div>
          )}
}

export default TermsnCondition;
