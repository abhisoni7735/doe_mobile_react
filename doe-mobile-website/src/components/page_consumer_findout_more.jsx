import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Navbar, Nav, NavDropdown, Form, FormControl, Button, NavItem } from "react-bootstrap";
import { Link, useLocation } from 'react-router-dom';
import doeLogo from "../assets/logo.png";
import Common_header from "./common_header";
import queryString from 'query-string';


class ConsumerFindOutMore extends Component {

  render(){
    let query = queryString.parse(this.props.location.search)
    this.tagSection = this.props.content['LANDING_PAGE']['HOME'].find( section => section['TAG'] == query.tag );
    console.log(this.tagSection);
    return(
      <div className="Consumer_findout_more consumer__section">
        <div className="find_out_more_conatiner">
          <Link to="/home">
            <img className="close_mark" src="./../assets/e-close.png" alt="" />
          </Link>

          <div className="findout_more_image_section">
            <img src={window.location.origin + this.tagSection.IMG_SRC} alt="" />
          </div>
          <div className="findout_more_text_container">
            <div className="findout_more_title" dangerouslySetInnerHTML={{ __html: this.tagSection.INTRO_LINE }}></div>
            <ul className="findout_more_points">
            {
              this.tagSection.CONTENT.map( (point, i) => <li key={i} dangerouslySetInnerHTML={{ __html: point }}></li> )
            }
            </ul>
          </div>
        </div>

      </div>
    )
  }
}
export default ConsumerFindOutMore;
