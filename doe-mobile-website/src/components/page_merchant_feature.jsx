import React, { Component } from 'react';
import { Navbar, Nav, NavDropdown, Form, FormControl, Button } from "react-bootstrap";
import { Link } from 'react-router-dom';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';

class MerchantFeatures extends Component {
  constructor(){
     super();
  }

  render(){
    const content = this.props.content;
    return(
      <div className="Merchant_main-section" id="merchant_features">
        <div className="merchant_feature_block">
        <Link to= {"/merchant/feature?name=" + Object.keys(this.props.content.MERCHANT.HOME)[0]}>
          <div className="merchant-feature_block_border">
            <div className="merchant_feature_block_img" >
              <img src="./../assets/merchant/payment-tap.png" alt=""/>
            </div>
          </div>
          <h4 className="feature_block_title"  dangerouslySetInnerHTML={{ __html: content.LANDING_PAGE.FEATURES.CONTENT_TABS.PAYMENTS.NAME }}></h4>
          <p className="feature_block_text" dangerouslySetInnerHTML={{ __html: content.LANDING_PAGE.FEATURES.CONTENT_TABS.PAYMENTS.SUMMARY }}></p>
          </Link>
        </div>
        <div className="merchant_feature_block">
          <Link to= {"/merchant/feature?name=" + Object.keys(this.props.content.MERCHANT.HOME)[1]}>
          <div className="merchant-feature_block_border">
            <div className="merchant_feature_block_img" >
              <img src="./../assets/merchant/access-management.png" alt=""/>
            </div>
          </div>
          <h4 className="feature_block_title"  dangerouslySetInnerHTML={{ __html: content.LANDING_PAGE.FEATURES.CONTENT_TABS.ACCESS.NAME }}></h4>
          <p className="feature_block_text" dangerouslySetInnerHTML={{ __html: content.LANDING_PAGE.FEATURES.CONTENT_TABS.ACCESS.SUMMARY }}></p>
          </Link>
        </div>
        <div className="merchant_feature_block">
        <Link to= {"/merchant/feature?name=" + Object.keys(this.props.content.MERCHANT.HOME)[2]}>
          <div className="merchant-feature_block_border">
            <div className="merchant_feature_block_img" >
              <img src="./../assets/merchant/merchant-loyalty1.png" alt=""/>
            </div>
          </div>
          <h4 className="feature_block_title"  dangerouslySetInnerHTML={{ __html: content.LANDING_PAGE.FEATURES.CONTENT_TABS.LOYALITY.NAME }}></h4>
          <p className="feature_block_text" dangerouslySetInnerHTML={{ __html: content.LANDING_PAGE.FEATURES.CONTENT_TABS.LOYALITY.SUMMARY }}></p>
          </Link>
        </div>
      </div>
    )
  }
}
export default MerchantFeatures;
