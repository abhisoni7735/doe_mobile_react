import React, { Component } from 'react';
import { Navbar, Nav, NavDropdown, Form, FormControl, Button } from "react-bootstrap";
import { Link } from 'react-router-dom';
import Carousel from 'react-bootstrap/Carousel'

class ConsumerServices extends Component {
  constructor(){
     super();
  }

  render(){
    return(
      <div className="Consumer_services_section consumer__section" id="consumer_services">
        <div className="section__ribbon">
          <p>Services</p>
        </div>
        <Carousel controls={false}>
          <Carousel.Item>
            <p className="carousel__title">Payments</p>
            <div className="services__circle_layout">
              <div className="service__circle_view toll_circle_view">
                <Link to= {"/home/service?name=" + Object.keys(this.props.content.LANDING_PAGE.SERVICES.CONTENT_TABS)[0]}>
                  <img className="service_non-hover" src="./../assets/services/toll-merchant-service.png" alt="" />
                  <img className="service_hover" src="./../assets/services/toll-merchant-service-hover.png" alt="" />
                </Link>
              </div>
              <div className="service__circle_view bus_circle_view">
                <Link to= {"/home/service?name=" + Object.keys(this.props.content.LANDING_PAGE.SERVICES.CONTENT_TABS)[4]}>
                  <img className="service_non-hover" src="./../assets/services/bus-merchant-service.png" alt="" />
                  <img className="service_hover" src="./../assets/services/bus-merchant-service-hover.png" alt="" />
                </Link>
              </div>
              <div className="service__circle_view parking_circle_view">
                <Link to= {"/home/service?name=" + Object.keys(this.props.content.LANDING_PAGE.SERVICES.CONTENT_TABS)[3]}>
                  <img className="service_non-hover" src="./../assets/services/parking-merchant-service.png" alt="" />
                  <img className="service_hover" src="./../assets/services/parking-merchant-service-hover.png" alt="" />
                </Link>
              </div>
              <div className="service__circle_view metro_circle_view">
                <Link to= {"/home/service?name=" + Object.keys(this.props.content.LANDING_PAGE.SERVICES.CONTENT_TABS)[1]}>
                  <img className="service_non-hover" src="./../assets/services/metro-merchant-service.png" alt="" />
                  <img className="service_hover" src="./../assets/services/metro-merchant-service-hover.png" alt="" />
                </Link>
              </div>
              <div className="service__circle_view railways_circle_view">
                <Link to= {"/home/service?name=" + Object.keys(this.props.content.LANDING_PAGE.SERVICES.CONTENT_TABS)[8]}>
                  <img className="service_non-hover" src="./../assets/services/railway-merchant-service.png" alt="" />
                  <img className="service_hover" src="./../assets/services/railway-merchant-service-hover.png" alt="" />
                </Link>
              </div>
              <div className="service__circle_view education_circle_view">
                <Link to= {"/home/service?name=" + Object.keys(this.props.content.LANDING_PAGE.SERVICES.CONTENT_TABS)[6]}>
                  <img className="service_non-hover" src="./../assets/services/edu-merchant-service.png" alt="" />
                  <img className="service_hover" src="./../assets/services/edu-merchant-service-hover.png" alt="" />
                </Link>
              </div>
              <div className="service__circle_view retail_circle_view">
                <Link to= {"/home/service?name=" + Object.keys(this.props.content.LANDING_PAGE.SERVICES.CONTENT_TABS)[2]}>
                  <img className="service_non-hover" src="./../assets/services/retail-merchant-service.png" alt="" />
                  <img className="service_hover" src="./../assets/services/retail-merchant-service-hover.png" alt="" />
                </Link>
              </div>
            </div>
            {/*<p className="slide_bottom_text">
              To pay at malls, airports & access parking lots
            </p>*/}
          </Carousel.Item>

          <Carousel.Item>
            <p className="carousel__title">Access</p>
            <div className="services__circle_layout">
              <div className="service__circle_view access__circle_view" >
                <Link to= {"/home/service?name=" + Object.keys(this.props.content.LANDING_PAGE.SERVICES.CONTENT_TABS)[9]}>
                  <img className="" src="./../assets/services/merchant-residency.png" alt=""/>
                </Link>
              </div>
              <div className="service__circle_view access__circle_view" >
                <Link to= {"/home/service?name=" + Object.keys(this.props.content.LANDING_PAGE.SERVICES.CONTENT_TABS)[10]}>
                  <img className="" src="./../assets/services/merchant-office-service.png" alt=""/>
                </Link>
              </div>
              <div className="service__circle_view access__circle_view " >
                <Link to= {"/home/service?name=" + Object.keys(this.props.content.LANDING_PAGE.SERVICES.CONTENT_TABS)[11]}>
                  <img className="" src="./../assets/services/merchant-edu-service.png" alt=""/>
                </Link>
              </div>
              {/*<div className="service__circle_view access__circle_view " >
                <Link to= {"/home/service?name=" + Object.keys(this.props.content.LANDING_PAGE.SERVICES.CONTENT_TABS)[0]}>
                  <img className="" src="./../assets/services/employe-merchant-service.png" alt=""/>
                </Link>
              </div>*/}
              <div className="service__circle_view access__circle_view ">
                <Link to= {"/home/service?name=" + Object.keys(this.props.content.LANDING_PAGE.SERVICES.CONTENT_TABS)[13]}>
                  <img className="" src="./../assets/services/merchant-hotels-service.png" alt=""/>
                </Link>
              </div>
              <div className="service__circle_view access__circle_view " >
                <Link to= {"/home/service?name=" + Object.keys(this.props.content.LANDING_PAGE.SERVICES.CONTENT_TABS)[12]}>
                  <img className="" src="./../assets/services/group-2803.png" alt=""/>
                </Link>
              </div>
              <div className="service__circle_view access__circle_view ">
                <Link to= {"/home/service?name=" + Object.keys(this.props.content.LANDING_PAGE.SERVICES.CONTENT_TABS)[14]}>
                  <img className="" src="./../assets/services/merchant-hospital-service.png" alt=""/>
                </Link>
              </div>
            </div>
            {/*<p className="slide_bottom_text">
              To pay at malls, airports & access parking lots
            </p>*/}
          </Carousel.Item>

          <Carousel.Item>
            <p className="carousel__title">Loyality</p>
            <div className="services__circle_layout">
              <div className="service__circle_view loylity__circle_view ">
                <Link to= {"/home/service?name=" + Object.keys(this.props.content.LANDING_PAGE.SERVICES.CONTENT_TABS)[15]}>
                  <img className="" src="./../assets/services/group-airlines.png" alt=""/>
                </Link>
              </div>
              <div className="service__circle_view loylity__circle_view ">
                <Link to= {"/home/service?name=" + Object.keys(this.props.content.LANDING_PAGE.SERVICES.CONTENT_TABS)[16]}>
                  <img className="" src="./../assets/services/group-clubs.png" alt=""/>
                </Link>
              </div>
              <div className="service__circle_view loylity__circle_view ">
                <Link to= {"/home/service?name=" + Object.keys(this.props.content.LANDING_PAGE.SERVICES.CONTENT_TABS)[17]}>
                  <img className="" src="./../assets/services/group-restarunt.png" alt=""/>
                </Link>
              </div>
              <div className="service__circle_view loylity__circle_view ">
                <Link to= {"/home/service?name=" + Object.keys(this.props.content.LANDING_PAGE.SERVICES.CONTENT_TABS)[18]}>
                  <img className="" src="./../assets/services/group-petro.png" alt=""/>
                </Link>
              </div>
            </div>
            {/*<p className="slide_bottom_text">
              To pay at malls, airports & access parking lots
            </p>*/}
          </Carousel.Item>



        </Carousel>
      </div>
    )
  }
}
export default ConsumerServices;
