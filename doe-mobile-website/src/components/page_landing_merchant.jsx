import React, { Component } from "react";
import { hot } from "react-hot-loader";
import ScrollableSection, { ScrollableLink } from 'react-update-url-on-scroll';
import Common_header from "./common_header.jsx";
import { Link } from 'react-router-dom';
import MerchantFeatures from "./page_merchant_feature";
import MerchantPayment from "./page_merchant_payment";
import MerchantAccess from "./page_merchant_access";
import MerchantLoyalty from "./page_merchant_loyalty";

class MerchantLanding extends Component {
  constructor(props){
    super(props);
  }

  render() {
    return (

      <div className="Merchant_main">
        <Common_header location={this.props.location}/>
        <MerchantFeatures content={this.props.content} />
        <MerchantPayment content={this.props.content} />
        <MerchantAccess content={this.props.content} />
        <MerchantLoyalty content={this.props.content} />
      </div>
    );
  }
}

export default MerchantLanding;
