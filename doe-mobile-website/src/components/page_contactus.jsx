import React, { Component } from "react";
import ReactDOM from 'react-dom';
import Common_header from "./common_header";
import { connect } from 'react-redux';
import Carousel from 'react-bootstrap/Carousel';
import { Link } from 'react-router-dom';

class Aboutus extends Component {



  render(){
    const content = this.props.content;
    return(
          <div className="contactus__page landing_page">
              <Common_header/>
              <div className="contact_section component">
                <div className="contact_img"><img src="./../assets/map.svg" /></div>
                <div className="contact_content">
                    <div className="contact_content_address">
                      <h4>DOE CARDS SOLUTIONS PVT. LTD</h4>
                      <div className="contact-address">
                        <p className="contact_content_head">Contact us</p>
                          <address dangerouslySetInnerHTML={{ __html: content.LANDING_PAGE.CONTACT_US.ADDRESS }}></address>
                        </div>
                    </div>
                    <div className="base-landing-page">
                      <div className="contact_content_agreement">
                        <Link to="/terms">TERMS OF SERVICE</Link>
                        <Link to="/privacy-policy">PRIVACY POLICY</Link>
                      </div>
                      <div className="contact_content_social">
                        <p>Follow us</p>
                        <div className="contact_content_social_img">
                          <a href={content?.LANDING_PAGE.CONTACT_US.SOCIAL_LINKS.TWITTER} rel="noopener noreferrer" target="_blank">
                            <img src="./../assets/twi.jpg"/>
                          </a>
                          <a href={content.LANDING_PAGE.CONTACT_US.SOCIAL_LINKS.FB} rel="noopener noreferrer" target="_blank">
                            <img src="./../assets/fab.jpg"/>
                          </a>
                          <a href={content.LANDING_PAGE.CONTACT_US.SOCIAL_LINKS.LINKEDIN}  rel="noopener noreferrer" target="_blank">
                            <img src="./../assets/linkd.jpg"/>
                          </a>
                          <a href={content.LANDING_PAGE.CONTACT_US.SOCIAL_LINKS.INSTAGRAM} rel="noopener noreferrer" target="_blank">
                            <img src="./../assets/insta.jpg"/>
                          </a>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
          </div>
        )
      }
  }



export default Aboutus;
