import React, { Component } from 'react';
import { Navbar, Nav, NavDropdown, Form, FormControl, Button } from "react-bootstrap";
import { Link } from 'react-router-dom';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';

class AboutDoeConsumer extends Component {
  constructor(){
     super();
  }

  Clickshowdiv () {

  }
  render(){
    const content = this.props.content;
    return(
      <div className="Consumer_main-section consumer__section" id="consumer_home">
        <Carousel className="carousel__home_page">

            {
              content.LANDING_PAGE.HOME.map( (slide, i) =>  <div key={i} className="consumer-sub-content">
                <div className="image--content">
                  <img src={window.location.origin + slide.IMG_SRC} />
                </div>
                <div className="first-content"  dangerouslySetInnerHTML={{ __html: slide.INTRO_LINE }}></div>
                <div className="second-content">
                  <span dangerouslySetInnerHTML={{ __html: slide.SUB_TITLE }}></span>
                </div>
                <div className="more-details-button">
                  <button><Link to={"/find-more?tag="+ slide.TAG}>FIND OUT MORE</Link></button>
                </div>
              </div>)

            }
        </Carousel>
      </div>
    )
  }
}
export default AboutDoeConsumer;
