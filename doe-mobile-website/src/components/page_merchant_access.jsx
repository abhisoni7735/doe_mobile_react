import React, { Component } from 'react';
import { Navbar, Nav, NavDropdown, Form, FormControl, Button } from "react-bootstrap";
import { Link } from 'react-router-dom';
import Carousel from 'react-bootstrap/Carousel'

class MerchantAccess extends Component {
  constructor(){
     super();
  }

  render(){
    return(
      <div className="Merchant_sub-section" id="merchant_access">
        <div className="section__ribbon">
          <p>Access</p>
        </div>
        <div className="service-merchant-image">
          <img src="./../assets/merchant/merchant-access.png" />
        </div>
        <div className="services__circle_layout">
          <div className="service__circle_view access__circle_view service__circle_view_mer" >
            <img className="" src="./../assets/services/merchant-residency.png" alt=""/>
          </div>
          <div className="service__circle_view access__circle_view service__circle_view_mer" >
            <img className="" src="./../assets/services/merchant-office-service.png" alt=""/>
          </div>
          <div className="service__circle_view access__circle_view service__circle_view_mer" >
            <img className="" src="./../assets/services/merchant-edu-service.png" alt=""/>
          </div>
          <div className="service__circle_view access__circle_view service__circle_view_mer" >
            <img className="" src="./../assets/services/employe-merchant-service.png" alt=""/>
          </div>
          <div className="service__circle_view access__circle_view service__circle_view_mer">
            <img className="" src="./../assets/services/merchant-hotels-service.png" alt=""/>
          </div>
          <div className="service__circle_view access__circle_view service__circle_view_mer" >
            <img className="" src="./../assets/services/group-2803.png" alt=""/>
          </div>
          <div className="service__circle_view access__circle_view service__circle_view_mer">
            <img className="" src="./../assets/services/merchant-hospital-service.png" alt=""/>
          </div>
        </div>
        <div className="more-details-button">
          <Link to="/access">
            <button className="mer_find_out">FIND OUT MORE</button>
          </Link>
        </div>
      </div>
    )
  }
}
export default MerchantAccess;
