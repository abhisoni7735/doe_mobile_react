import React, { Component } from 'react';
import { Navbar, Nav, NavDropdown, Form, FormControl, Button } from "react-bootstrap";
import doeLogo from "../assets/logo.png";
import { Link } from 'react-router-dom';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
import {
  Menu,
  MenuList,
  MenuButton,
  MenuItem,
  MenuItems,
  MenuPopover,
  MenuLink,
} from "@reach/menu-button";
import "@reach/menu-button/styles.css";

class Common_header extends Component {
  constructor(props){
    super(props);
    this.state = {
      showndiv: false,
      selectedMenu: '',
      selectedMerchanttype:'LANDING',
      showpaymentsMenu: true
    }
  }

  componentDidMount(){
    var merchantScreens = ['toll', 'parking', 'retail', 'bus', 'education','railway', 'metro', 'access' , 'loyalty'];

    if(this.props.location){

      if(this.props.location.pathname.indexOf('home') > -1){
        this.setState({
          selectedMenu : 'CONSUMER'
        })
      }else if(this.props.location.pathname.indexOf('corporate') > -1){
        this.setState({
          selectedMenu : 'CORPORATE'
        })
      }else if(this.contains(this.props.location.pathname, merchantScreens)){
        this.setState({
          selectedMenu : 'MERCHANT',
          selectedMerchanttype:'SECTION'
        })
      }else if(this.props.location.pathname.indexOf('merchant') > -1){
        this.setState({
          selectedMenu : 'MERCHANT',
          selectedMerchanttype:'LANDING'
        })
      }else{
        this.setState({
          selectedMenu : ''
        })
      }

    }else{
      this.setState({
        selectedMenu : ''
      })
    }
  }

   contains(target, pattern){
      var value = 0;
      pattern.forEach(function(word){
        value = value + target.includes(word);
      });
      return (value === 1)
  }

  Clickshowdiv () {
    this.setState({
      showndiv : true,
    })
  }

  clickPaymentsMenu(){
    this.setState({
      showpaymentsMenu : !this.state.showpaymentsMenu
    })
  }

  render(){
    return(
      <div className="main_header common_headr">
        <div className="header_layout">
          <div className="header_layout__logo">
            <Link to ="/"><img src="./../assets/logo.png" alt="logo"/></Link>
          </div>
          <a className= {"a-service-category" + (this.state.selectedMenu == "CONSUMER" ? " active" : "")}>
             <Menu>
              <MenuButton>
                CONSUMER
              </MenuButton>
              <MenuList>
                <MenuItem onSelect={() => console.log("Download")}><a href="/home#consumer_home">HOME</a></MenuItem>
                <MenuItem onSelect={() => console.log("Download")}><a href="/home#consumer_features">FEATURES</a></MenuItem>
                <MenuItem onSelect={() => console.log("Download")}><a href="/home#consumer_scope">SCOPE</a></MenuItem>
                <MenuItem onSelect={() => console.log("Download")}><a href="/home#consumer_services">SERVICE</a></MenuItem>
                <MenuItem onSelect={() => console.log("Download")}><a href="/home#consumer_joinus">JOIN US</a></MenuItem>
                <MenuItem onSelect={() => console.log("Download")}><a href="/home#consumer_videos">VIDEOS</a></MenuItem>
                <MenuItem onSelect={() => console.log("Download")}><a href="/home#consumer_faqs">FAQS</a></MenuItem>
              </MenuList>
            </Menu>
          </a>
          <a className= {"a-service-category" + (this.state.selectedMenu == "MERCHANT" ? " active" : "")}>
            <Menu>
              <MenuButton>
                BUSINESS
              </MenuButton>
              {
                (this.state.selectedMerchanttype == "LANDING") ? <MenuList>
                  <MenuItem onSelect={() => console.log("Download")}><a href="/merchant#merchant_features">HOME</a></MenuItem>
                  <MenuItem onSelect={() => console.log("Download")}><a href="/merchant#merchant_payment">PAYMENTS</a></MenuItem>
                  <MenuItem onSelect={() => console.log("Download")}><a href="/merchant#merchant_access">ACCESS</a></MenuItem>
                  <MenuItem onSelect={() => console.log("Download")}><a href="/merchant#merchant_loyalty">LOYALITY</a></MenuItem>
                </MenuList> :
                <MenuList className="merchant__submenu_list">
                  <div className="drop_down_menu">
                    <div className="drop_down_menu__payments">
                      <div className="payments_drop_down" onClick={() => this.clickPaymentsMenu()}>
                        <p>payments</p>
                        {this.state.showpaymentsMenu ? <img src="./../assets/drop-down-arrow.png" alt="arrow" /> : <img src="./../assets/drop-up-arrow.png" alt="arrow" /> }

                      </div>
                      {this.state.showpaymentsMenu ? <div className="payments_submenu_list">
                        <Link to="/toll">
                          <img src="./../assets/toll/toll_main_logo.png" alt=""/>
                        </Link>
                        <Link to="/bus">
                          <img src="./../assets/bus/bus_main_logo.png" alt=""/>
                        </Link>
                        <Link to="/parking">
                          <img src="./../assets/parking/parking_main_logo.png" alt=""/>
                        </Link>
                        <Link to="/education">
                          <img src="./../assets/education/edu_main_logo.png" alt=""/>
                        </Link>
                        <Link to="/retail">
                          <img src="./../assets/retail/retail_main_logo.png" alt=""/>
                        </Link>
                        <Link to="/metro">
                          <img src="./../assets/metro/metro_main_logo.png" alt=""/>
                        </Link>
                        <Link to="/railway">
                          <img src="./../assets/railway/railway_main_logo.png" alt=""/>
                        </Link>
                      </div> : null}

                    </div>
                    <Link to="/access">
                      <div className="drop_down_menu__access">
                        <p>Acccess</p>
                      </div>
                    </Link>

                    <Link to="/loyalty">
                      <div className="drop_down_menu__loyality">
                        <p>Loyality</p>
                      </div>
                    </Link>
                  </div>
                </MenuList>
              }
            </Menu>
          </a>
          <a className= {"a-service-category" + (this.state.selectedMenu == "CORPORATE" ? " active" : "")} >
            <Menu>
              <MenuButton>
                CORPORATE
              </MenuButton>
              <MenuList className="corporate__menu_list">
                <MenuItem onSelect={() => console.log("Download")}><a href="/corporate#corporate_home">HOME</a></MenuItem>
                <MenuItem onSelect={() => console.log("Download")}><a href="/corporate#corporate_joinus">JOIN US</a></MenuItem>
                <MenuItem onSelect={() => console.log("Download")}><a href="/corporate#corporate_videos">VIDEOS</a></MenuItem>
              </MenuList>

            </Menu>
          </a>
          {
            this.state.showndiv ? null :  <Navbar expand="lg">
              <Navbar.Toggle aria-controls="basic-navbar-nav" onClick={() => this.Clickshowdiv()}/>
            </Navbar>
          }
          {
           this.state.showndiv ?<button className="overlaymenubutton" onClick={() => this.setState({showndiv : false})}> X </button> : null
          }

        </div>

        {
          this.state.showndiv ?
          <div className="overlaymenu">
            <div className="overlaymenu__list">
              <div className="overlaymenu__list">
                <p><Link to="/joinus">JOIN US</Link></p>
                <p><Link to="/contactus">CONTACT US</Link></p>
                <p><Link to="/aboutus">ABOUT US</Link></p>
                <p><Link to="/partners">PARTNERS</Link></p>
              </div>
            </div>
            <div className="footer-modal-section">
              <p>Copyright © 2018. All Rights Reserved By</p>
              <span> DOE CARDS SOLUTIONS PVT. LTD</span>
            </div>
          </div> : null
        }
      </div>
    )
  }
}
export default Common_header;
