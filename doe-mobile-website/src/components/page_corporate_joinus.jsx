import React, { Component } from 'react';
import { Navbar, Nav, NavDropdown, Form, FormControl, Button } from "react-bootstrap";
import { Link } from 'react-router-dom';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';

class CorporateJoinus extends Component {
  constructor(){
     super();
  }

  Clickshowdiv () {

  }
  render(){
    return(
      <div className="Corp-main-div" id="corporate_joinus">
        <div className="banner-green">
          <span>JOIN US </span>
        </div>
        <div className="logo-button-div">
        <Link to="/merchant/joinus">
          <div className="log-button-sub-div">
            <img src="./../assets/corporate/corp-device.png" />
            <button>Signup as Merchant
              <img src="./../assets/corporate/right_arrow.png" />
            </button>
          </div>
          </Link>
        </div>
        <div className="logo-button-div">
        <Link to="/joinus">
          <div className="log-button-sub-div">
            <img src="./../assets/corporate/corp-doe-cards.png" />
            <button>Want a DOE Card?
            <img src="./../assets/corporate/right_arrow.png" />
            </button>

          </div>
          </Link>
        </div>
      </div>
    )
  }
}
export default CorporateJoinus;
