import React, { Component } from 'react';
import { Navbar, Nav, NavDropdown, Form, FormControl, Button } from "react-bootstrap";
import { Link } from 'react-router-dom';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';

class ConsumerFeatures extends Component {
  constructor(){
     super();
  }

  render(){
    const content = this.props.content;
    return(
      <div className="Consumer_feature_section consumer__section" id="consumer_features">
        <div className="section__ribbon">
          <p>Feature</p>
        </div>
        <h3 className="sub_section_title" dangerouslySetInnerHTML={{ __html: content.LANDING_PAGE.FEATURES.TAG_LINE }}></h3>
        <div className="consumer_feature_block">
          <Link to= {"/home/feature?name=" + Object.keys(this.props.content.LANDING_PAGE.FEATURES.CONTENT_TABS)[0]}>
          <div className="consumer_feature_block_img" >
            <img src="./../assets/consumer/payment-feature.png" alt=""/>
          </div>
          <h4 className="feature_block_title"  dangerouslySetInnerHTML={{ __html: content.LANDING_PAGE.FEATURES.CONTENT_TABS.PAYMENTS.NAME }}></h4>
          <p className="feature_block_text" dangerouslySetInnerHTML={{ __html: content.LANDING_PAGE.FEATURES.CONTENT_TABS.PAYMENTS.SUMMARY }}></p>
          </Link>
        </div>
        <div className="consumer_feature_block">
          <Link to= {"/home/feature?name=" + Object.keys(this.props.content.LANDING_PAGE.FEATURES.CONTENT_TABS)[1]}>
          <div className="consumer_feature_block_img" >
            <img src="./../assets/consumer/access-feature.png" alt=""/>
          </div>
          <h4 className="feature_block_title"  dangerouslySetInnerHTML={{ __html: content.LANDING_PAGE.FEATURES.CONTENT_TABS.ACCESS.NAME }}></h4>
          <p className="feature_block_text" dangerouslySetInnerHTML={{ __html: content.LANDING_PAGE.FEATURES.CONTENT_TABS.ACCESS.SUMMARY }}></p>
          </Link>
        </div>
        <div className="consumer_feature_block">
          <Link to= {"/home/feature?name=" + Object.keys(this.props.content.LANDING_PAGE.FEATURES.CONTENT_TABS)[2]}>
          <div className="consumer_feature_block_img" >
            <img src="./../assets/consumer/loyalty-feature.png" alt=""/>
          </div>
          <h4 className="feature_block_title"  dangerouslySetInnerHTML={{ __html: content.LANDING_PAGE.FEATURES.CONTENT_TABS.LOYALITY.NAME }}></h4>
          <p className="feature_block_text" dangerouslySetInnerHTML={{ __html: content.LANDING_PAGE.FEATURES.CONTENT_TABS.LOYALITY.SUMMARY }}></p>
          </Link>
        </div>
      </div>
    )
  }
}
export default ConsumerFeatures;
