import React, { Component } from 'react';
import { Navbar, Nav, NavDropdown, Form, FormControl, Button } from "react-bootstrap";
import { Link } from 'react-router-dom';
import Carousel from 'react-bootstrap/Carousel'

class ConsumerScope extends Component {
  render(){
    return(
      <div className="Consumer_scope_section consumer__section" id="consumer_scope">
        <div className="section__ribbon">
          <p>Scope</p>
        </div>
        <Carousel controls={false}>
          <Carousel.Item>
            <div className="consumer__scope__slide">
              <p className="consumer__scope__head metro_text_color">METRO</p>
              <img src="./../assets/scope/metro.png" />
              <p className="consumer__scope__summary">Tap to enter, pay & exit across all metros in the country.</p>
            </div>
          </Carousel.Item>
          <Carousel.Item>
            <div className="consumer__scope__slide">
              <p className="consumer__scope__head residence_text_color">RESIDENCE</p>
              <img src="./../assets/scope/residence.png" />
              <p className="consumer__scope__summary">Enter your gated community and homes.</p>
            </div>
          </Carousel.Item>
          <Carousel.Item>
            <div className="consumer__scope__slide">
              <p className="consumer__scope__head education_text_color">EDUCATION</p>
              <img src="./../assets/scope/education.png" />
              <p className="consumer__scope__summary">Use Doe Card as your ID card and attendance marking.</p>
            </div>
          </Carousel.Item>
          <Carousel.Item>
            <div className="consumer__scope__slide">
              <p className="consumer__scope__head bus_text_color">BUS</p>
              <img src="./../assets/scope/bus.png" />
              <p className="consumer__scope__summary">As a bus pass and pay for all corporations across the country.</p>
            </div>
          </Carousel.Item>
          <Carousel.Item>
            <div className="consumer__scope__slide">
              <p className="consumer__scope__head office_text_color">OFFICE</p>
              <img src="./../assets/scope/office.png" />
              <p className="consumer__scope__summary">As employee ID & to log in log out.</p>
            </div>
          </Carousel.Item>
          <Carousel.Item>
            <div className="consumer__scope__slide">
              <p className="consumer__scope__head parking_text_color">PARKING</p>
              <img src="./../assets/scope/parking.png" />
              <p className="consumer__scope__summary">To pay at malls, airports & access parking lots.</p>
            </div>
          </Carousel.Item>
          <Carousel.Item>
            <div className="consumer__scope__slide">
              <p className="consumer__scope__head retail_text_color">RETAIL</p>
              <img src="./../assets/scope/retail.png" />
              <p className="consumer__scope__summary">Payments and building loyalty points.</p>
            </div>
          </Carousel.Item>
          <Carousel.Item>
            <div className="consumer__scope__slide">
              <p className="consumer__scope__head toll_text_color">TOLL</p>
              <img src="./../assets/scope/toll.png" />
              <p className="consumer__scope__summary">To pay across all tolls in the country</p>
            </div>
          </Carousel.Item>

        </Carousel>
        <h3 className="sub_section_title">
          <img src="./../assets/scope/swipe-left.svg" />
        </h3>
      </div>
    )
  }
}
export default ConsumerScope;
