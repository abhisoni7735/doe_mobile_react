import React, { Component } from 'react';
import { Navbar, Nav, NavDropdown, Form, FormControl, Button } from "react-bootstrap";

import { Link } from 'react-router-dom';
import Common_header from "../../common_header";

class MerchantRailway extends Component {
  constructor(props){
    super(props);
  }

  render(){
    return(
      <div className="Merchant_service_railway_main">
        <Common_header location={this.props.location}/>
        <div className="services_section_intro">
          <div className="services_common_logo">
            <img src="./../../../assets/railway/railway-logo-colored.png" alt="" />
            <p className="railways_text_color">RAILWAY</p>
          </div>
          <div className="services__intro_toll_contnet">
            <h3 dangerouslySetInnerHTML={{ __html: this.props.content.RAILWAYS.ABOUT.INTRO_LINE }}></h3>
            <p className="services_intro_sub_text" dangerouslySetInnerHTML={{ __html: this.props.content.RAILWAYS.ABOUT.SUB_TITLE }}></p>
            {/*<span className="want_to_merchant toll_text_color">Want to become an DOE merchant?</span>*/}
            <img src="./../../../assets/railway/railway.png" alt="toll" />
          </div>
        </div>

        <div className="services_section_feature" id="features">
          <p className="services_section_title">Features</p>
          <div className="feature_section_view">
            {/* <Link to= {"/railway/feature-info?feature=" + Object.keys(this.props.content.RAILWAYS.FEATURES.CONTENT_TABS)[0]}> */}
              <div className="img_view_section">
                <img src="./../../../assets/railway/nfc.png" alt="" />
              </div>
              <h4 className="feture__title" dangerouslySetInnerHTML={{ __html: this.props.content.RAILWAYS.FEATURES.CONTENT_TABS.PAPERLESS_TICKETING.NAME }}></h4>
              <p className="feature__contnet" dangerouslySetInnerHTML={{ __html: this.props.content.RAILWAYS.FEATURES.CONTENT_TABS.PAPERLESS_TICKETING.SUMMARY }}></p>
          {/* </Link> */}
          </div>

          <div className="feature_section_view">
              {/* <Link to= {"/railway/feature-info?feature=" + Object.keys(this.props.content.RAILWAYS.FEATURES.CONTENT_TABS)[1]}> */}
              <div className="img_view_section">
                <img src="./../../../assets/railway/multiple-ticket.png" alt="" />
              </div>
              <h4 className="feture__title" dangerouslySetInnerHTML={{ __html: this.props.content.RAILWAYS.FEATURES.CONTENT_TABS.TRAVEL_PASS.NAME }}></h4>
              <p className="feature__contnet" dangerouslySetInnerHTML={{ __html: this.props.content.RAILWAYS.FEATURES.CONTENT_TABS.TRAVEL_PASS.SUMMARY }}></p>
              {/* </Link> */}
          </div>

          <div className="feature_section_view">
            {/* <Link to= {"/railway/feature-info?feature=" + Object.keys(this.props.content.RAILWAYS.FEATURES.CONTENT_TABS)[2]}> */}
              <div className="img_view_section">
                <img src="./../../../assets/railway/ticket.png" alt="" />
              </div>
              <h4 className="feture__title" dangerouslySetInnerHTML={{ __html: this.props.content.RAILWAYS.FEATURES.CONTENT_TABS.FLEET_ROUTE_MANAGEMENT.NAME }}></h4>
              <p className="feature__contnet" dangerouslySetInnerHTML={{ __html: this.props.content.RAILWAYS.FEATURES.CONTENT_TABS.FLEET_ROUTE_MANAGEMENT.SUMMARY }}></p>
            {/* </Link> */}
          </div>

          <div className="feature_section_view">
            {/* <Link to= {"/railway/feature-info?feature=" + Object.keys(this.props.content.RAILWAYS.FEATURES.CONTENT_TABS)[3]}> */}
              <div className="img_view_section">
                <img src="./../../../assets/railway/train-station.png" alt="" />
              </div>
              <h4 className="feture__title" dangerouslySetInnerHTML={{ __html: this.props.content.RAILWAYS.FEATURES.CONTENT_TABS.EMPLOYED_ID_ACCESS.NAME }}></h4>
              <p className="feature__contnet" dangerouslySetInnerHTML={{ __html: this.props.content.RAILWAYS.FEATURES.CONTENT_TABS.EMPLOYED_ID_ACCESS.SUMMARY }}></p>
            {/* </Link> */}
          </div>

        </div>

        <div className="services_section_products" id="products">
          <p className="services_section_title">Products</p>
            <div className="procucts__cards_layout" >
              {
                Object.entries(this.props.device).map( (device, i) =>  <Link key={i} to= {"/product-info?device=" + device[0]  }> <div  className="procucts__card cursor">
                  <div className="procuct__image">
                    <img src={window.location.origin + device[1].image} alt=""/>
                  </div>
                  <div className="product__details">
                    <h4 className="Product_title" dangerouslySetInnerHTML={{ __html: device[1].name }}></h4>
                    <p className="product__description" dangerouslySetInnerHTML={{ __html: device[1].summary }}></p>
                    {/*<h3 className="prdct_cost"><span className="rupee_symbol_font">&#8377;</span></h3>*/}
                  </div>
                </div></Link>)
              }
            </div>
        </div>

      </div>
    )
  }
}
export default MerchantRailway;
