import React, { Component } from 'react';
import { Navbar, Nav, NavDropdown, Form, FormControl, Button } from "react-bootstrap";

import { Link } from 'react-router-dom';
import Common_header from "../../common_header";

class MerchantAccess extends Component {
  constructor(props){
    super(props);
  }

  render(){
    return(
      <div className="Merchant_service_access_main">
        <Common_header location={this.props.location}/>
        <div className="services_section_intro">
          <div className="services_toll_logo">
            <img src="./../../../assets/access/access-logo-colored.png" alt="" />
            <p className="access_text_color">Access</p>
          </div>
          <div className="services__intro_toll_contnet">
            <h3 dangerouslySetInnerHTML={{ __html: this.props.content.ACCESS.ABOUT.INTRO_LINE }}></h3>
            <p className="services_intro_sub_text" dangerouslySetInnerHTML={{ __html: this.props.content.ACCESS.ABOUT.SUB_TITLE }}></p>
            {/*<span className="want_to_merchant toll_text_color">Want to become an DOE merchant?</span>*/}
            <img src="./../../../assets/access/access.png" alt="toll" />
          </div>
        </div>

        <div className="services_section_products" id="products">
          <p className="services_section_title">Products</p>
            <div className="procucts__cards_layout" >
              {
                Object.entries(this.props.device).map( (device, i) =>  <Link key={i} to= {"/product-info?device=" + device[0]  }> <div  className="procucts__card cursor">
                  <div className="procuct__image">
                    <img src={window.location.origin + device[1].image} alt=""/>
                  </div>
                  <div className="product__details">
                    <h4 className="Product_title" dangerouslySetInnerHTML={{ __html: device[1].name }}></h4>
                    <p className="product__description" dangerouslySetInnerHTML={{ __html: device[1].summary }}></p>
                    {/*<h3 className="prdct_cost"><span className="rupee_symbol_font">&#8377;</span></h3>*/}
                  </div>
                </div></Link>)
              }
            </div>
        </div>

        <div className="services_section_service_types" id="services">
          <p className="services_section_title">Services</p>
            <div className="services__sections__layout four_layer_service_section_box">


                <div className="srvice_section_box">
                  <Link to= {"/access/service-info?service=" + Object.keys(this.props.content.ACCESS.SERVICES)[6]}>
                    <div className="service__section access_service__section cursor" >
                      <div className="service_procuct__image">
                        <img src="./../../../assets/services/access-residence.png" alt=""/>
                      </div>
                    </div>
                    <p>RESIDENCE</p>
                    </Link>
                </div>



                <div className="srvice_section_box">
                    <Link to= {"/access/service-info?service=" + Object.keys(this.props.content.ACCESS.SERVICES)[2]}>
                    <div className="service__section access_service__section cursor" >
                      <div className="service_procuct__image">
                        <img src="./../../../assets/services/access-offices.png" alt=""/>
                      </div>
                    </div>
                    <p>OFFICES</p>
                    </Link>
                </div>



                <div className="srvice_section_box">
                  <Link to= {"/access/service-info?service=" + Object.keys(this.props.content.ACCESS.SERVICES)[0]}>
                    <div className="service__section access_service__section cursor" >
                      <div className="service_procuct__image">
                        <img src="./../../../assets/services/access-education.png" alt=""/>
                      </div>
                    </div>
                    <p>EDUCATIONAL INSTITUTES</p>
                      </Link>
                </div>



                <div className="srvice_section_box">
                <Link to= {"/access/service-info?service=" + Object.keys(this.props.content.ACCESS.SERVICES)[3]}>
                  <div className="service__section access_service__section cursor" >
                    <div className="service_procuct__image">
                      <img src="./../../../assets/services/access-hotel.png" alt=""/>
                    </div>
                  </div>
                  <p>HOTELS</p>
                  </Link>
                </div>



                <div className="srvice_section_box">
                    <Link to= {"/access/service-info?service=" + Object.keys(this.props.content.ACCESS.SERVICES)[1]}>
                  <div className="service__section access_service__section cursor" >
                    <div className="service_procuct__image">
                      <img src="./../../../assets/services/access-apartment.png" alt=""/>
                    </div>
                  </div>
                  <p>APARTMENTS / GATED COMMUNITY</p>
                  </Link>
                </div>



                <div className="srvice_section_box">
                  <Link to= {"/access/service-info?service=" + Object.keys(this.props.content.ACCESS.SERVICES)[5]}>
                  <div className="service__section access_service__section cursor" >
                    <div className="service_procuct__image">
                      <img src="./../../../assets/services/access-hospital.png" alt=""/>
                    </div>
                  </div>
                  <p>HOSPITALS</p>
                  </Link>
                </div>

            </div>

        </div>




      </div>
    )
  }
}


export default MerchantAccess;
