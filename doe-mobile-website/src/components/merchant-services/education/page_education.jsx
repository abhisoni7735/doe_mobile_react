import React, { Component } from 'react';
import { Navbar, Nav, NavDropdown, Form, FormControl, Button } from "react-bootstrap";

import { Link } from 'react-router-dom';
import Common_header from "../../common_header";

class MerchantEducation extends Component {
  constructor(props){
    super(props);
  }

  render(){
    return(
      <div className="Merchant_service_education_main">
        <Common_header location={this.props.location}/>
        <div className="services_section_intro">
          <div className="services_common_logo">
            <img src="./../../../assets/education/education-logo-colored.png" alt="" />
            <p className="education_text_color">EDUCATION</p>
          </div>
          <div className="services__intro_toll_contnet">
            <h3 dangerouslySetInnerHTML={{ __html: this.props.content.EDUCATION.ABOUT.INTRO_LINE }}></h3>
            <p className="services_intro_sub_text" dangerouslySetInnerHTML={{ __html: this.props.content.EDUCATION.ABOUT.SUB_TITLE }}></p>
            {/*<span className="want_to_merchant toll_text_color">Want to become an DOE merchant?</span>*/}
            <img src="./../../../assets/education/education.png" alt="toll" />
          </div>
        </div>

        <div className="services_section_feature" id="features">
          <p className="services_section_title">Features</p>
          <div className="feature_section_view">
            <Link to= {"/education/feature-info?feature=" + Object.keys(this.props.content.EDUCATION.FEATURES.CONTENT_TABS)[0]}>
              <div className="img_view_section">
                <img src="./../../../assets/education/card.png" alt="" />
              </div>
              <h4 className="feture__title" dangerouslySetInnerHTML={{ __html: this.props.content.EDUCATION.FEATURES.CONTENT_TABS.SECURE_ACCESS.NAME }}></h4>
              <p className="feature__contnet" dangerouslySetInnerHTML={{ __html: this.props.content.EDUCATION.FEATURES.CONTENT_TABS.SECURE_ACCESS.SUMMARY }}></p>
            </Link>
          </div>

          <div className="feature_section_view">
            <Link to= {"/education/feature-info?feature=" + Object.keys(this.props.content.EDUCATION.FEATURES.CONTENT_TABS)[1]}>
              <div className="img_view_section">
                <img src="./../../../assets/education/multiple.png" alt="" />
              </div>
              <h4 className="feture__title" dangerouslySetInnerHTML={{ __html: this.props.content.EDUCATION.FEATURES.CONTENT_TABS.MULTIPLE_ACCESS.NAME }}></h4>
              <p className="feature__contnet" dangerouslySetInnerHTML={{ __html: this.props.content.EDUCATION.FEATURES.CONTENT_TABS.MULTIPLE_ACCESS.SUMMARY }}></p>
            </Link>
          </div>

          <div className="feature_section_view">
            <Link to= {"/education/feature-info?feature=" + Object.keys(this.props.content.EDUCATION.FEATURES.CONTENT_TABS)[2]}>
              <div className="img_view_section">
                <img src="./../../../assets/education/payment-non-hover.png" alt="" />
              </div>
              <h4 className="feture__title" dangerouslySetInnerHTML={{ __html: this.props.content.EDUCATION.FEATURES.CONTENT_TABS.TOPUP_PAYMENTS.NAME }}></h4>
              <p className="feature__contnet" dangerouslySetInnerHTML={{ __html: this.props.content.EDUCATION.FEATURES.CONTENT_TABS.TOPUP_PAYMENTS.SUMMARY }}></p>
            </Link>
          </div>

          <div className="feature_section_view">
            <Link to= {"/education/feature-info?feature=" + Object.keys(this.props.content.EDUCATION.FEATURES.CONTENT_TABS)[3]}>
              <div className="img_view_section">
                <img src="./../../../assets/education/id-icon.png" alt="" />
              </div>
              <h4 className="feture__title" dangerouslySetInnerHTML={{ __html: this.props.content.EDUCATION.FEATURES.CONTENT_TABS.PRINTABLE_ID.NAME }}></h4>
              <p className="feature__contnet" dangerouslySetInnerHTML={{ __html: this.props.content.EDUCATION.FEATURES.CONTENT_TABS.PRINTABLE_ID.SUMMARY }}></p>
            </Link>
          </div>

        </div>
<div className="services_section_service_types" id="services">
          <p className="services_section_title">Services</p>
            <div className="services__sections__layout four_layer_service_section_box">
              <div className="srvice_section_box">
                <div className="service__section bus_service__section cursor" >
                  <div className="service_procuct__image">
                    <img src="./../../../assets/education/bus.png" alt=""/>
                  </div>
                </div>
                <p>BUS</p>
              </div>
              <div className="srvice_section_box">
                <div className="service__section bus_service__section cursor" >
                  <div className="service_procuct__image">
                    <img src="./../../../assets/education/mansion.png" alt=""/>
                  </div>
                </div>
                <p>HOSTEL</p>
              </div>
              <div className="srvice_section_box">
                <div className="service__section bus_service__section cursor" >
                  <div className="service_procuct__image">
                    <img src="./../../../assets/education/canteen.png" alt=""/>
                  </div>
                </div>
                <p>Canteen</p>
              </div>
              <div className="srvice_section_box">
                <div className="service__section bus_service__section cursor" >
                  <div className="service_procuct__image">
                    <img src="./../../../assets/education/restricted.png" alt=""/>
                  </div>
                </div>
                <p>Restricted Access</p>
              </div>
              <div className="srvice_section_box">
                <div className="service__section bus_service__section cursor" >
                  <div className="service_procuct__image">
                    <img src="./../../../assets/education/bike.png" alt=""/>
                  </div>
                </div>
                <p>Parking</p>
              </div>
            </div>

        </div>

        <div className="services_section_products" id="products">
          <p className="services_section_title">Products</p>
            <div className="procucts__cards_layout" >
              {
                Object.entries(this.props.device).map( (device, i) =>  <Link key={i} to= {"/product-info?device=" + device[0]  }> <div  className="procucts__card cursor">
                  <div className="procuct__image">
                    <img src={window.location.origin + device[1].image} alt=""/>
                  </div>
                  <div className="product__details">
                    <h4 className="Product_title" dangerouslySetInnerHTML={{ __html: device[1].name }}></h4>
                    <p className="product__description" dangerouslySetInnerHTML={{ __html: device[1].summary }}></p>
                    {/*<h3 className="prdct_cost"><span className="rupee_symbol_font">&#8377;</span></h3>*/}
                  </div>
                </div></Link>)
              }
            </div>
        </div>

      </div>
    )
  }
}
export default MerchantEducation;
