import React, { Component } from 'react';
import { Navbar, Nav, NavDropdown, Form, FormControl, Button } from "react-bootstrap";
import { Link } from 'react-router-dom';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
import {
  Menu,
  MenuList,
  MenuButton,
  MenuItem,
  MenuItems,
  MenuPopover,
  MenuLink,
} from "@reach/menu-button";
import "@reach/menu-button/styles.css";

class Common_header extends Component {
  constructor(){
    super();
    this.state = {
      showndiv: false,
    }
  }

  Clickshowdiv () {
    this.setState({
      showndiv : true,
    })
  }

  render(){
    return(
      <div className="main_header common_headr">
        <div className="header_layout">
          <div className="header_layout__logo">
            <Link to ="/"><img src="./../../assets/logo.png" alt="logo"/></Link>
          </div>
          <a className="a-service-category">
             <Menu>
              <MenuButton>
                PAYMENTS
              </MenuButton>
              <MenuList>
                <MenuItem onSelect={() => console.log("Download")}>TOLL</MenuItem>
                <MenuItem onSelect={() => console.log("Download")}>BUS</MenuItem>
                <MenuItem onSelect={() => console.log("Download")}>PARKING</MenuItem>
                <MenuItem onSelect={() => console.log("Download")}>EDUCATION</MenuItem>
                <MenuItem onSelect={() => console.log("Download")}> RETAIL</MenuItem>
                <MenuItem onSelect={() => console.log("Download")}>METRO</MenuItem>
                <MenuItem onSelect={() => console.log("Download")}>RAILWAYS</MenuItem>
              </MenuList>
            </Menu>
          </a>
          <a className="a-service-category">
            <Menu>
              <MenuButton>
                ACCESS
              </MenuButton>

            </Menu>
          </a>
          <a className="a-service-category">
            <Menu>
              <MenuButton>
                LOYALITY
              </MenuButton>

            </Menu>
          </a>
          {
            this.state.showndiv ? null :  <Navbar expand="lg">
              <Navbar.Toggle aria-controls="basic-navbar-nav" onClick={() => this.Clickshowdiv()}/>
            </Navbar>
          }
          {
           this.state.showndiv ?<button className="overlaymenubutton" onClick={() => this.setState({showndiv : false})}> X </button> : null
          }

        </div>

        {
          this.state.showndiv ?
          <div className="overlaymenu">
            <div className="overlaymenu__list">
              <p><Link to="/joinus">JOIN US</Link></p>
              <p><Link to="/contactus">CONTACT US</Link></p>
              <p><Link to="/aboutus">ABOUT US</Link></p>
              <p><Link to="/joinus">PATNERS</Link></p>
            </div>
            <div className="footer-modal-section">
              <p>Copyright © 2018. All Rights Reserved By</p>
              <span> DOE CARDS SOLUTIONS PVT. LTD</span>
            </div>
          </div> : null
        }
      </div>
    )
  }
}
export default Common_header;
