import React, { Component } from 'react';
import { Navbar, Nav, NavDropdown, Form, FormControl, Button } from "react-bootstrap";

import { Link } from 'react-router-dom';
import Common_header from "../../common_header";

class MerchantLoyalty extends Component {
  constructor(props){
    super(props);
  }

  render(){
    return(
      <div className="Merchant_service_loyalty_main">
        <Common_header location={this.props.location}/>
        <div className="services_section_intro">
          <div className="services_toll_logo">
            <img src="./../../../assets/loyalty/loyalty-logo-colored.png" alt="" />
            <p className="loyalty_text_color">Loyalty</p>
          </div>
          <div className="services__intro_toll_contnet">
            <h3 dangerouslySetInnerHTML={{ __html: this.props.content.LOYALTY.ABOUT.INTRO_LINE }}></h3>
            <p className="services_intro_sub_text" dangerouslySetInnerHTML={{ __html: this.props.content.LOYALTY.ABOUT.SUB_TITLE }}></p>
            {/*<span className="want_to_merchant toll_text_color">Want to become an DOE merchant?</span>*/}
            <img src="./../../../assets/loyalty/loyalty.png" alt="toll" />
          </div>
        </div>

        <div className="services_section_products" id="products">
          <p className="services_section_title">Products</p>
            <div className="procucts__cards_layout" >
              {
                Object.entries(this.props.device).map( (device, i) =>  <Link key={i} to= {"/product-info?device=" + device[0]  }> <div  className="procucts__card cursor">
                  <div className="procuct__image">
                    <img src={window.location.origin + device[1].image} alt=""/>
                  </div>
                  <div className="product__details">
                    <h4 className="Product_title" dangerouslySetInnerHTML={{ __html: device[1].name }}></h4>
                    <p className="product__description" dangerouslySetInnerHTML={{ __html: device[1].summary }}></p>
                    {/*<h3 className="prdct_cost"><span className="rupee_symbol_font">&#8377;</span></h3>*/}
                  </div>
                </div></Link>)
              }
            </div>
        </div>

        <div className="services_section_service_types" id="services">
          <p className="services_section_title">Services</p>
            <div className="services__sections__layout four_layer_service_section_box">
                <div className="srvice_section_box">
                  <Link to= {"/LOYALTY/service-info?service=" + Object.keys(this.props.content.LOYALTY.SERVICES)[1]}>
                    <div className="service__section loyalty_service__section cursor" >
                      <div className="service_procuct__image">
                        <img src="./../../../assets/services/loyalty-airlines.png" alt=""/>
                      </div>
                    </div>
                    <p>AIRLINES</p>
                    </Link>
                </div>
                <div className="srvice_section_box">
                    <Link to= {"/LOYALTY/service-info?service=" + Object.keys(this.props.content.LOYALTY.SERVICES)[2]}>
                    <div className="service__section loyalty_service__section cursor" >
                      <div className="service_procuct__image">
                        <img src="./../../../assets/services/loyalty-clubs.png" alt=""/>
                      </div>
                    </div>
                    <p>CLUBS</p>
                    </Link>
                </div>
                <div className="srvice_section_box">
                  <Link to= {"/LOYALTY/service-info?service=" + Object.keys(this.props.content.LOYALTY.SERVICES)[0]}>
                    <div className="service__section loyalty_service__section cursor" >
                      <div className="service_procuct__image">
                        <img src="./../../../assets/services/loyalty-restuarent.png" alt=""/>
                      </div>
                    </div>
                    <p>RESTUARENTS</p>
                      </Link>
                </div>
                <div className="srvice_section_box">
                <Link to= {"/LOYALTY/service-info?service=" + Object.keys(this.props.content.LOYALTY.SERVICES)[4]}>
                  <div className="service__section loyalty_service__section cursor" >
                    <div className="service_procuct__image">
                      <img src="./../../../assets/services/loyalty-petrol.png" alt=""/>
                    </div>
                  </div>
                  <p>PETRO CARDS</p>
                  </Link>
                </div>
            </div>

        </div>



      </div>
    )
  }
}


export default MerchantLoyalty;
