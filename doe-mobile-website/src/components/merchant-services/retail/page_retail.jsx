import React, { Component } from 'react';
import { Navbar, Nav, NavDropdown, Form, FormControl, Button } from "react-bootstrap";

import { Link } from 'react-router-dom';
import Common_header from "../../common_header";

class MerchantRetail extends Component {
  constructor(props){
    super(props);
  }

  render(){
    return(
      <div className="Merchant_service_retail_main">
        <Common_header location={this.props.location}/>
        <div className="services_section_intro">
          <div className="services_toll_logo">
            <img src="./../../../assets/retail/retail-logo-colored.png" alt="" />
            <p className="retail_text_color">Retail</p>
          </div>
          <div className="services__intro_toll_contnet">
            <h3 dangerouslySetInnerHTML={{ __html: this.props.content.RETAIL.ABOUT.INTRO_LINE }}></h3>
            <p className="services_intro_sub_text" dangerouslySetInnerHTML={{ __html: this.props.content.RETAIL.ABOUT.SUB_TITLE }}></p>
            {/*<span className="want_to_merchant toll_text_color">Want to become an DOE merchant?</span>*/}
            <img src="./../../../assets/retail/retail.png" alt="toll" />
          </div>
        </div>

        <div className="services_section_feature" id="features">
          <p className="services_section_title">Features</p>
          <div className="feature_section_view">
            <Link to= {"/retail/feature-info?feature=" + Object.keys(this.props.content.RETAIL.FEATURES.CONTENT_TABS)[0]}>
              <div className="img_view_section">
                <img src="./../../../assets/retail/loyalty.png" alt="" />
              </div>
              <h4 className="feture__title" dangerouslySetInnerHTML={{ __html: this.props.content.RETAIL.FEATURES.CONTENT_TABS.CUSTOMER_MANAGEMENT_POS.NAME }}></h4>
              <p className="feature__contnet" dangerouslySetInnerHTML={{ __html: this.props.content.RETAIL.FEATURES.CONTENT_TABS.CUSTOMER_MANAGEMENT_POS.SUMMARY }}></p>
            </Link>
          </div>


          <div className="feature_section_view">
            <Link to= {"/retail/feature-info?feature=" + Object.keys(this.props.content.RETAIL.FEATURES.CONTENT_TABS)[1]}>
              <div className="img_view_section">
                <img src="./../../../assets/retail/flash.png" alt="" />
              </div>
              <h4 className="feture__title" dangerouslySetInnerHTML={{ __html: this.props.content.RETAIL.FEATURES.CONTENT_TABS.FAST_TRANSACTION_PAYMENTS.NAME }}></h4>
              <p className="feature__contnet" dangerouslySetInnerHTML={{ __html: this.props.content.RETAIL.FEATURES.CONTENT_TABS.FAST_TRANSACTION_PAYMENTS.SUMMARY }}></p>
            </Link>
          </div>

          <div className="feature_section_view">
            <Link to= {"/retail/feature-info?feature=" + Object.keys(this.props.content.RETAIL.FEATURES.CONTENT_TABS)[2]}>
              <div className="img_view_section">
                <img src="./../../../assets/retail/multiple-utility.png" alt="" />
              </div>
              <h4 className="feture__title" dangerouslySetInnerHTML={{ __html: this.props.content.RETAIL.FEATURES.CONTENT_TABS.MULTIPLE_UTILITIES.NAME }}></h4>
              <p className="feature__contnet" dangerouslySetInnerHTML={{ __html: this.props.content.RETAIL.FEATURES.CONTENT_TABS.MULTIPLE_UTILITIES.SUMMARY }}></p>
            </Link>
          </div>

          <div className="feature_section_view">
            <Link to= {"/retail/feature-info?feature=" + Object.keys(this.props.content.RETAIL.FEATURES.CONTENT_TABS)[3]}>
              <div className="img_view_section">
                <img src="./../../../assets/retail/secure.png" alt="" />
              </div>
              <h4 className="feture__title" dangerouslySetInnerHTML={{ __html: this.props.content.RETAIL.FEATURES.CONTENT_TABS.SECURE_PAYMENTS.NAME }}></h4>
              <p className="feature__contnet" dangerouslySetInnerHTML={{ __html: this.props.content.RETAIL.FEATURES.CONTENT_TABS.SECURE_PAYMENTS.SUMMARY }}></p>
            </Link>
          </div>

        </div>

        <div className="services_section_service_types" id="services">
          <p className="services_section_title">Services</p>
            <div className="services__sections__layout four_layer_service_section_box">
              <div className="srvice_section_box">
                <div className="service__section parking_service__section cursor" >
                  <div className="service_procuct__image">
                    <img src="./../../../assets/services/retail-restaurant.png" alt=""/>
                  </div>
                </div>
                <p>Restaurant</p>
              </div>
              <div className="srvice_section_box">
                <div className="service__section parking_service__section cursor" >
                  <div className="service_procuct__image">
                    <img src="./../../../assets/services/retail-fuel.png" alt=""/>
                  </div>
                </div>
                <p>Fuel Station</p>
              </div>
              <div className="srvice_section_box">
                <div className="service__section parking_service__section cursor" >
                  <div className="service_procuct__image">
                    <img src="./../../../assets/services/retail-taxi.png" alt=""/>
                  </div>
                </div>
                <p>Taxi</p>
              </div>
              <div className="srvice_section_box">
                <div className="service__section parking_service__section cursor" >
                  <div className="service_procuct__image">
                    <img src="./../../../assets/services/malls.png" alt=""/>
                  </div>
                </div>
                <p>Super Market</p>
              </div>
              <div className="srvice_section_box">
                <div className="service__section parking_service__section cursor" >
                  <div className="service_procuct__image">
                    <img src="./../../../assets/services/retail-vending.png" alt=""/>
                  </div>
                </div>
                <p>Vending Michanes</p>
              </div>
              <div className="srvice_section_box">
                <div className="service__section parking_service__section cursor" >
                  <div className="service_procuct__image">
                    <img src="./../../../assets/services/retail-shops.png" alt=""/>
                  </div>
                </div>
                <p>Small Shops</p>
              </div>
            </div>

        </div>

        <div className="services_section_products" id="products">
          <p className="services_section_title">Products</p>
            <div className="procucts__cards_layout" >
              {
                Object.entries(this.props.device).map( (device, i) =>  <Link key={i} to= {"/product-info?device=" + device[0]  }> <div  className="procucts__card cursor">
                  <div className="procuct__image">
                    <img src={window.location.origin + device[1].image} alt=""/>
                  </div>
                  <div className="product__details">
                    <h4 className="Product_title" dangerouslySetInnerHTML={{ __html: device[1].name }}></h4>
                    <p className="product__description" dangerouslySetInnerHTML={{ __html: device[1].summary }}></p>
                    {/*<h3 className="prdct_cost"><span className="rupee_symbol_font">&#8377;</span></h3>*/}
                  </div>
                </div></Link>)
              }
            </div>
        </div>
      </div>
    )
  }
}


export default MerchantRetail;
