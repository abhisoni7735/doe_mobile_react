import React, { Component } from 'react';
import { Navbar, Nav, NavDropdown, Form, FormControl, Button } from "react-bootstrap";

import { Link } from 'react-router-dom';
import Common_header from "../../common_header";

class MerchantBus extends Component {
  constructor(props){
    super(props);
  }

  render(){
    return(
      <div className="Merchant_service_bus_main">
        <Common_header location={this.props.location}/>
        <div className="services_section_intro">
          <div className="services_common_logo">
            <img src="./../../../assets/bus/bus-logo-colored.png" alt="" />
            <p className="bus_text_color">BUS</p>
          </div>
          <div className="services__intro_toll_contnet">
            <h3 dangerouslySetInnerHTML={{ __html: this.props.content.BUS.ABOUT.INTRO_LINE }}></h3>
            <p className="services_intro_sub_text" dangerouslySetInnerHTML={{ __html: this.props.content.BUS.ABOUT.SUB_TITLE }}></p>
            {/*<span className="want_to_merchant toll_text_color">Want to become an DOE merchant?</span>*/}
            <img src="./../../../assets/bus/bus.png" alt="toll" />
          </div>
        </div>

        <div className="services_section_feature" id="features">
          <p className="services_section_title">Features</p>
          <div className="feature_section_view">
            <Link to= {"/bus/feature-info?feature=" + Object.keys(this.props.content.BUS.FEATURES.CONTENT_TABS)[0]}>
              <div className="img_view_section">
                <img src="./../../../assets/bus/ticketing-non-hover.png" alt="" />
              </div>
              <h4 className="feture__title" dangerouslySetInnerHTML={{ __html: this.props.content.BUS.FEATURES.CONTENT_TABS.PAPERLESS_TICKETING.NAME }}></h4>
              <p className="feature__contnet" dangerouslySetInnerHTML={{ __html: this.props.content.BUS.FEATURES.CONTENT_TABS.PAPERLESS_TICKETING.SUMMARY }}></p>
            </Link>
          </div>

          <div className="feature_section_view">
            <Link to= {"/bus/feature-info?feature=" + Object.keys(this.props.content.BUS.FEATURES.CONTENT_TABS)[1]}>
              <div className="img_view_section">
                <img src="./../../../assets/bus/travel-passes-non-hover.png" alt="" />
              </div>
              <h4 className="feture__title" dangerouslySetInnerHTML={{ __html: this.props.content.BUS.FEATURES.CONTENT_TABS.TRAVEL_PASS.NAME }}></h4>
              <p className="feature__contnet" dangerouslySetInnerHTML={{ __html: this.props.content.BUS.FEATURES.CONTENT_TABS.TRAVEL_PASS.SUMMARY }}></p>
            </Link>
          </div>

          <div className="feature_section_view">
            <Link to= {"/bus/feature-info?feature=" + Object.keys(this.props.content.BUS.FEATURES.CONTENT_TABS)[2]}>
              <div className="img_view_section">
                <img src="./../../../assets/bus/track-non-hover.png" alt="" />
              </div>
              <h4 className="feture__title" dangerouslySetInnerHTML={{ __html: this.props.content.BUS.FEATURES.CONTENT_TABS.FLEET_ROUTE_MANAGEMENT.NAME }}></h4>
              <p className="feature__contnet" dangerouslySetInnerHTML={{ __html: this.props.content.BUS.FEATURES.CONTENT_TABS.FLEET_ROUTE_MANAGEMENT.SUMMARY }}></p>
            </Link>
          </div>

          <div className="feature_section_view">
            <Link to= {"/bus/feature-info?feature=" + Object.keys(this.props.content.BUS.FEATURES.CONTENT_TABS)[3]}>
              <div className="img_view_section">
                <img src="./../../../assets/bus/id-non-hover.png" alt="" />
              </div>
              <h4 className="feture__title" dangerouslySetInnerHTML={{ __html: this.props.content.BUS.FEATURES.CONTENT_TABS.EMPLOYED_ID_ACCESS.NAME }}></h4>
              <p className="feature__contnet" dangerouslySetInnerHTML={{ __html: this.props.content.BUS.FEATURES.CONTENT_TABS.EMPLOYED_ID_ACCESS.SUMMARY }}></p>
            </Link>
          </div>

        </div>

        <div className="services_section_service_types" id="services">
          <p className="services_section_title">Services</p>
            <div className="services__sections__layout four_layer_service_section_box">
              <div className="srvice_section_box">
                <div className="service__section bus_service__section cursor" >
                  <div className="service_procuct__image">
                    <img src="./../../../assets/bus/bus-3-corporation.png" alt=""/>
                  </div>
                </div>
                <p>Corporation</p>
              </div>
              <div className="srvice_section_box">
                <div className="service__section bus_service__section cursor" >
                  <div className="service_procuct__image">
                    <img src="./../../../assets/bus/bus-private.png" alt=""/>
                  </div>
                </div>
                <p>Private</p>
              </div>
              <div className="srvice_section_box">
                <div className="service__section bus_service__section cursor" >
                  <div className="service_procuct__image">
                    <img src="./../../../assets/bus/bus-7-inter.png" alt=""/>
                  </div>
                </div>
                <p>Inter state</p>
              </div>
              <div className="srvice_section_box">
                <div className="service__section bus_service__section cursor" >
                  <div className="service_procuct__image">
                    <img src="./../../../assets/bus/bus-8-state.png" alt=""/>
                  </div>
                </div>
                <p>Intra State</p>
              </div>
              <div className="srvice_section_box">
                <div className="service__section bus_service__section cursor" >
                  <div className="service_procuct__image">
                    <img src="./../../../assets/bus/bus-9-intra.png" alt=""/>
                  </div>
                </div>
                <p>Intra city</p>
              </div>
              <div className="srvice_section_box">
                <div className="service__section bus_service__section cursor" >
                  <div className="service_procuct__image">
                    <img src="./../../../assets/bus/school-bus.png" alt=""/>
                  </div>
                </div>
                <p>School Buses</p>
              </div>
            </div>

        </div>

        <div className="services_section_products" id="products">
          <p className="services_section_title">Products</p>
            <div className="procucts__cards_layout" >
              {
                Object.entries(this.props.device).map( (device, i) =>  <Link key={i} to= {"/product-info?device=" + device[0]  }> <div  className="procucts__card cursor">
                  <div className="procuct__image">
                    <img src={window.location.origin + device[1].image} alt=""/>
                  </div>
                  <div className="product__details">
                    <h4 className="Product_title" dangerouslySetInnerHTML={{ __html: device[1].name }}></h4>
                    <p className="product__description" dangerouslySetInnerHTML={{ __html: device[1].summary }}></p>
                    {/*<h3 className="prdct_cost"><span className="rupee_symbol_font">&#8377;</span></h3>*/}
                  </div>
                </div></Link>)
              }
            </div>
        </div>

      </div>
    )
  }
}
export default MerchantBus;
