import React, { Component } from 'react';
import { Navbar, Nav, NavDropdown, Form, FormControl, Button } from "react-bootstrap";

import { Link } from 'react-router-dom';
import Common_header from "../../common_header";

class MerchantParking extends Component {
  constructor(props){
    super(props);
  }

  render(){
    return(
      <div className="Merchant_service_parking_main">
        <Common_header location={this.props.location}/>
        <div className="services_section_intro">
          <div className="services_common_logo">
            <img src="./../../../assets/parking/parking-logo-colored.png" alt="" />
            <p className="parking_text_color">PARKING</p>
          </div>
          <div className="services__intro_toll_contnet">
            <h3 dangerouslySetInnerHTML={{ __html: this.props.content.PARKING.ABOUT.INTRO_LINE }}></h3>
            <p className="services_intro_sub_text" dangerouslySetInnerHTML={{ __html: this.props.content.PARKING.ABOUT.SUB_TITLE }}></p>
            {/*<span className="want_to_merchant toll_text_color">Want to become an DOE merchant?</span>*/}
            <img src="./../../../assets/parking/parking.png" alt="toll" />
          </div>
        </div>

        <div className="services_section_feature" id="features">
          <p className="services_section_title">Features</p>
          <div className="feature_section_view">
            <Link to= {"/parking/feature-info?feature=" + Object.keys(this.props.content.PARKING.FEATURES.CONTENT_TABS)[0]}>
              <div className="img_view_section">
                <img src="./../../../assets/parking/easy-parking.png" alt="" />
              </div>
              <h4 className="feture__title" dangerouslySetInnerHTML={{ __html: this.props.content.PARKING.FEATURES.CONTENT_TABS.EASY_PARKING.NAME }}></h4>
              <p className="feature__contnet" dangerouslySetInnerHTML={{ __html: this.props.content.PARKING.FEATURES.CONTENT_TABS.EASY_PARKING.SUMMARY }}></p>
            </Link>
          </div>

          <div className="feature_section_view">
            <Link to= {"/parking/feature-info?feature=" + Object.keys(this.props.content.PARKING.FEATURES.CONTENT_TABS)[1]}>
              <div className="img_view_section">
                <img src="./../../../assets/parking/secure.png" alt="" />
              </div>
              <h4 className="feture__title" dangerouslySetInnerHTML={{ __html: this.props.content.PARKING.FEATURES.CONTENT_TABS.INCREASED_SECURITY.NAME }}></h4>
              <p className="feature__contnet" dangerouslySetInnerHTML={{ __html: this.props.content.PARKING.FEATURES.CONTENT_TABS.INCREASED_SECURITY.SUMMARY }}></p>
            </Link>
          </div>

          <div className="feature_section_view">
            <Link to= {"/parking/feature-info?feature=" + Object.keys(this.props.content.PARKING.FEATURES.CONTENT_TABS)[2]}>
              <div className="img_view_section">
                <img src="./../../../assets/parking/faster.png" alt="" />
              </div>
              <h4 className="feture__title" dangerouslySetInnerHTML={{ __html: this.props.content.PARKING.FEATURES.CONTENT_TABS.FASTER_ALLOCATION.NAME }}></h4>
              <p className="feature__contnet" dangerouslySetInnerHTML={{ __html: this.props.content.PARKING.FEATURES.CONTENT_TABS.FASTER_ALLOCATION.SUMMARY }}></p>
            </Link>
          </div>

          <div className="feature_section_view">
            <Link to= {"/parking/feature-info?feature=" + Object.keys(this.props.content.PARKING.FEATURES.CONTENT_TABS)[3]}>
              <div className="img_view_section">
                <img src="./../../../assets/parking/processing-time.png" alt="" />
              </div>
              <h4 className="feture__title" dangerouslySetInnerHTML={{ __html: this.props.content.PARKING.FEATURES.CONTENT_TABS.HASSLE_FREE_PROCESSING.NAME }}></h4>
              <p className="feature__contnet" dangerouslySetInnerHTML={{ __html: this.props.content.PARKING.FEATURES.CONTENT_TABS.HASSLE_FREE_PROCESSING.SUMMARY }}></p>
            </Link>
          </div>

        </div>

        <div className="services_section_service_types" id="services">
          <p className="services_section_title">Services</p>
            <div className="services__sections__layout four_layer_service_section_box">
              <div className="srvice_section_box">
                <div className="service__section parking_service__section cursor" >
                  <div className="service_procuct__image">
                    <img src="./../../../assets/services/parking-airport.png" alt=""/>
                  </div>
                </div>
                <p>AIRPORT</p>
              </div>
              <div className="srvice_section_box">
                <div className="service__section parking_service__section cursor" >
                  <div className="service_procuct__image">
                    <img src="./../../../assets/services/parking-railways.png" alt=""/>
                  </div>
                </div>
                <p>RAILWAY</p>
              </div>
              <div className="srvice_section_box">
                <div className="service__section parking_service__section cursor" >
                  <div className="service_procuct__image">
                    <img src="./../../../assets/services/parking-taxi.png" alt=""/>
                  </div>
                </div>
                <p>On Street Parking</p>
              </div>
              <div className="srvice_section_box">
                <div className="service__section parking_service__section cursor" >
                  <div className="service_procuct__image">
                    <img src="./../../../assets/services/temples.png" alt=""/>
                  </div>
                </div>
                <p>Temples</p>
              </div>
              <div className="srvice_section_box">
                <div className="service__section parking_service__section cursor" >
                  <div className="service_procuct__image">
                    <img src="./../../../assets/services/malls.png" alt=""/>
                  </div>
                </div>
                <p>Malls</p>
              </div>
              <div className="srvice_section_box">
                <div className="service__section parking_service__section cursor" >
                  <div className="service_procuct__image">
                    <img src="./../../../assets/services/parking-street.png" alt=""/>
                  </div>
                </div>
                <p>Commercial Street Parking</p>
              </div>
            </div>

        </div>

        <div className="services_section_products" id="products">
          <p className="services_section_title">Products</p>
            <div className="procucts__cards_layout" >
              {
                Object.entries(this.props.device).map( (device, i) =>  <Link key={i} to= {"/product-info?device=" + device[0]  }> <div  className="procucts__card cursor">
                  <div className="procuct__image">
                    <img src={window.location.origin + device[1].image} alt=""/>
                  </div>
                  <div className="product__details">
                    <h4 className="Product_title" dangerouslySetInnerHTML={{ __html: device[1].name }}></h4>
                    <p className="product__description" dangerouslySetInnerHTML={{ __html: device[1].summary }}></p>
                    {/*<h3 className="prdct_cost"><span className="rupee_symbol_font">&#8377;</span></h3>*/}
                  </div>
                </div></Link>)
              }
            </div>
        </div>

      </div>
    )
  }
}


export default MerchantParking;
