import React, { Component } from 'react';
import { Navbar, Nav, NavDropdown, Form, FormControl, Button } from "react-bootstrap";
import { Link } from 'react-router-dom';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
import {
  Menu,
  MenuList,
  MenuButton,
  MenuItem,
  MenuItems,
  MenuPopover,
  MenuLink,
} from "@reach/menu-button";
import "@reach/menu-button/styles.css";
import queryString from 'query-string';

class ProductDetails extends Component {
  goback(){
    this.props.history.goBack();
  }
  render(){
    let query = queryString.parse(this.props.location.search);
    console.log(query);

    return(
      <div className="merchant_product_info_page">
          <div className="close__mark">
            {/*<Link to="/toll"><img src="./../../assets/close-x.png" alt="close" /></Link>*/}
            <img src="./../../assets/close-x.png" alt="close" onClick={() => this.goback()} />
          </div>
        <div className="product_info_image">
          <img src={window.location.origin + this.props.device[query.device].image} alt=""/>
        </div>
        <div className="about_product_info">
          <h4 dangerouslySetInnerHTML={{ __html: this.props.device[query.device].name }}></h4>
          <p dangerouslySetInnerHTML={{ __html: this.props.device[query.device].summary }}></p>
        </div>
        <div className="product_description">
          <h4>Product Description</h4>
          <p dangerouslySetInnerHTML={{ __html: this.props.device[query.device].description }}></p>
        </div>
        <div className="product_Services_info">
          <h4>Product Support services</h4>
          <p>Toll, Retail, Metro, Parking, Bus, Education, Residency, & Office…..</p>
          {/*<span>Estimated delivery with in India 3-5 days</span>*/}
          <div className="product_more_info">
            <div class="product__information">
              <div class="product__information_details">
                <h4 class="information_details__key">Product Information</h4>
              </div>
              {
                this.props.device[query.device].productInformation.map( (info, j) => <div key={j}class="product__information_details">
                  <p class="information_details__key" dangerouslySetInnerHTML={{ __html: info.keyName }}></p>
                  <p class="information_details__value" dangerouslySetInnerHTML={{ __html: info.value }}></p>
                </div> )
              }
              <div class="note__points">
                {
                  this.props.device[query.device].productInformationKeypoints.map( (point, k) =>   <p key={k}>  *	{point}</p> )
                }
              </div>
            </div>
            <div class="product__information">
              <div class="product__information_details">
                <h4 class="information_details__key">Other Information</h4>
              </div>
              {
                this.props.device[query.device].otherSpecifications.map( (info, k) => <div key={k}class="product__information_details">
                  <p class="information_details__key" dangerouslySetInnerHTML={{ __html: info.keyName }}></p>
                  <p class="information_details__value" dangerouslySetInnerHTML={{ __html: info.value }}></p>
                </div> )
              }
              <div class="note__points">
                {
                  this.props.device[query.device].otherSpecificationsKeypoints.map( (point, l) =>   <p key={l}>  *	{point}</p> )
                }
              </div>
              <div class="note__points">
                {
                  this.props.device[query.device].termsCOnditions.map( (point, m) =>   <p key={m}>  *	{point}</p> )
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
export default ProductDetails;
