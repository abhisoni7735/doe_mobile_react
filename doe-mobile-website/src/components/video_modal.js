import React from 'react';
import PropTypes from 'prop-types';

class VideoModal extends React.Component {
  render() {
    // Render nothing if the "show" prop is false
    if(!this.props.show) {
      return null;
    }

    return (
      <div className="modal__popup ">
        <div className="modal__popup_container ">
          <div className= "modal__popup_content_section ">
            {this.props.children}

            <div className="popup_footer">
              <button className="popup_action_button slide-fwd-center" onClick={this.props.onClose}>
                Close
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

VideoModal.propTypes = {
  onClose: PropTypes.func.isRequired,
  show: PropTypes.bool,
  children: PropTypes.node
};

export default VideoModal;
