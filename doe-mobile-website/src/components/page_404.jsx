import React, { Component } from "react";
import { Link } from 'react-router-dom';

class PageNotFound extends Component {
  render(){
    return(
      <div class="page_not_found__layout">
  <div class="page_not_found__layout__images">
    <img class="page_not_found_number" src="./../assets/doe-404.png" />
    <img class="page_not_found__doe_card" src="./../assets/404.png" />
  </div>
  <p>Sorry,Page not found</p>
  <button type="button" name="button" href="">
	  <Link to="/home">
	  Go to Home page</Link>
	</button>
</div>

	  
    )
  }
}

export default PageNotFound;
