import React, { Component } from "react";

import ReactDOM from 'react-dom';
import Common_header from "./common_header.jsx";
import { connect } from 'react-redux';
import Carousel from 'react-bootstrap/Carousel';

class Aboutus extends Component {

  render(){
     this.state = {
      thirdTeamArray : []
    }
    const content = this.props.content;
    const deviceDetails = this.props.device;
     let firstTeamArray = [];
     let secondTeamArray = [];
      firstTeamArray.push(content.ABOUT.MANAGEMENT_TEAM[0],content.ABOUT.MANAGEMENT_TEAM[1]);
      secondTeamArray.push(content.ABOUT.MANAGEMENT_TEAM[2],content.ABOUT.MANAGEMENT_TEAM[3]);
      this.state.thirdTeamArray.push(firstTeamArray,secondTeamArray);
      let arrDevives = [];
      {Object.entries(deviceDetails).map((arr,i) => {
        console.log("arr iss",arr)
        arrDevives.push(arr)
      })}
    return(
          <div className="aboutsus__page">
              <Common_header/>
            <div className="page_layout__container">
              <div className="aboutus_bgimage">
                <img src="./../assets/Mask Copy.png" alt="point-of-sale-systems"/>
                <h3>Doe Cards Solutions Pvt.Ltd</h3>
              </div>
              <div className="aboutus__content">
                <div className="skwed">
                  About Us
                </div>
                <div className="aboutus__content__image">
                  <img className="" src="./../assets/doe-card-bg.png" alt="doe-card-bg"/>
                    <div className="aboutus__content__text">
                      <h4 dangerouslySetInnerHTML={{ __html: content.ABOUT.SUMMARY }}></h4>
                      <p dangerouslySetInnerHTML={{ __html: content.ABOUT.DESCRIPTION }}></p>
                    </div>
                </div>
              </div>
              <div className="aboutus__scroll">
                <h3>Our Projects</h3>
                <div className="aboutus__scroll_section">
                  <Carousel controls={false}>
                  {arrDevives.map((details, j) => {
                    return <Carousel.Item key={j}>
                      <div className="corusel_content">
                        <div className="corusel_image_slider">
                          <img  src={window.location.origin + details[1].image} alt="corusel-img" />
                        </div>
                        <h1 dangerouslySetInnerHTML={{ __html: details[1].name }}></h1>
                        <p dangerouslySetInnerHTML={{ __html: details[1].summary }}></p>
                        <p dangerouslySetInnerHTML={{ __html: details[1].description }}></p>
                      </div>
                    </Carousel.Item>
                     })}
                  </Carousel>
                </div>
              </div>
              <div className="our_team_section">
                <h3>Our Team</h3>
                <p className="our_team_contnet">

                </p>
                <div className="our_team__scroll">
                  <Carousel controls={false}>

                   <Carousel.Item >
                       <div className="corusel_content">
                        <div className="corusel_image_slider">
                        {this.state.thirdTeamArray[0].map((details, j) => {
                          return <div className="team_member_profile" key={j}>
                            <div className="team__corusel_image">
                              <div className="team_member_image">
                                <img  src={window.location.origin + details.IMAGE} />
                              </div>
                            </div>
                            <div className="member_profile_details">
                              <h4 dangerouslySetInnerHTML={{ __html: details.NAME }}></h4>
                              <p dangerouslySetInnerHTML={{ __html: details.DESIGNATION }}></p>
                            </div>
                          </div>
                           })}
                        </div>
                      </div>

                    </Carousel.Item>
                    <Carousel.Item >
                       <div className="corusel_content">
                        <div className="corusel_image_slider">
                        {this.state.thirdTeamArray[1].map((details, j) => {

                           return <div className="team_member_profile" key={j}>
                            <div className="team__corusel_image">
                              <div className="team_member_image">
                                <img  src={window.location.origin + details.IMAGE} />
                              </div>
                            </div>
                            <div className="member_profile_details">
                              <h4 dangerouslySetInnerHTML={{ __html: details.NAME }}></h4>
                              <p dangerouslySetInnerHTML={{ __html: details.DESIGNATION }}></p>
                            </div>
                          </div>
                           })}
                        </div>
                      </div>
                    </Carousel.Item>
                  </Carousel>
                </div>
              </div>
              <div className="our_team_section">
                <h3 style={{width: "139px"}}>Our Advisors</h3>
                <p className="our_team_contnet">

                </p>
                <div className="our_team__scroll">
                       <div className="corusel_content">
                        <div className="corusel_image_slider">
                        {content.ABOUT.ADVISORS.map((details, j) => {
                          return <div className="team_member_profile" key={j}>
                            <div className="team__corusel_image">
                              <div className="team_member_image">
                                <img  src={window.location.origin + details.IMAGE} />
                              </div>
                            </div>
                            <div className="member_profile_details">
                              <h4 dangerouslySetInnerHTML={{ __html: details.NAME }}></h4>
                              <p dangerouslySetInnerHTML={{ __html: details.DESIGNATION }}></p>
                            </div>
                          </div>
                           })}
                        </div>
                      </div>
                </div>
              </div>

            </div>
          </div>


    )
    }
  }



export default Aboutus;
