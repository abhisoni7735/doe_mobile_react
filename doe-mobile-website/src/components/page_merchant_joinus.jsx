import React, { Component } from 'react';
import { Navbar, Nav, NavDropdown, Form, FormControl, Button } from "react-bootstrap";
import { Link } from 'react-router-dom';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
import { connect } from 'react-redux';
import {PropTypes}  from 'prop-types';
import {newuser}    from '../actions/userActions.js'
import { userService } from '../services/joinus.service.js';
import Modal from './modal';

import Recaptcha from 'react-recaptcha';


const validEmailRegex = RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);
const validcompanyRegex = RegExp(/^([a-zA-Z0-9\s ]{3,32})$/i);
const validGstRegex = RegExp(/^([0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[0-9]{1}Z[a-zA-Z0-9]{1})$/i);
const validMobileRegex = RegExp(/^([0-9]{10})$/i);
const validNameRegex = RegExp(/^([a-zA-Z\s ]{3,32})$/i);
const validateForm = (errors) => {
  let valid = true;
  if( !validNameRegex.test(errors.name) || !validcompanyRegex.test(errors.companyName) || !validMobileRegex.test(errors.mobile) || !document.getElementById("join-us-form").reportValidity()){
    valid = false;
  }
  return valid;
}
// create a variable to store the component instance


class MerchantJoinus extends Component {
  // constructor(){
  //    super();
  //   this.state = {
  //     showndiv: false,
  //   }
  // }

  Clickshowdiv () {
    this.setState({
      showndiv : true,
    })
  }

  goback(){
    this.props.history.goBack();
  }

  _isMounted = false;
  constructor(props){
    super(props);
    this.state = {
      name: '',
      email: '',
      mobile: '',
      companyName:'',
      comments: '',
      userType: 'MERCHANT',
      gst : '',
      businessType : '',
      address : '',
      comments: '',
      showModal:true,
      errors: {
        name: '',
        email: '',
        mobile: '',
        companyName:'',
        gst : '',
        businessType : '',
        address : '',
      },
      isOpen: false,
      isCaptchaverified : false,
      formminavlid : '',
      captchamsg : ''
    };
     this.onRadioChange = this.onRadioChange.bind(this);
     this.handleSubmit = this.handleSubmit.bind(this);
     this.toggleModal= this.toggleModal.bind(this);
     this.captchcallback = this.captchcallback.bind(this);
     this.verifyCallback = this.verifyCallback.bind(this);

  }

  captchcallback() {
    console.log('captcha loaded');
  }

  verifyCallback(response){
    console.log(response);
    if(response) {
      this.setState({
        isCaptchaverified : true
      })
    }
  }

  toggleModal = () => {
    console.log('this is trigerd for popu[]')
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  close = () => {
    this.setState({ showModal: false });
  }

  open = () => {
    this.setState({ showModal: true });
  }

  handleChange = (event) => {
    event.preventDefault();
    const { name, value } = event.target;
    let errors = this.state.errors;

    switch (name) {
      case 'companyName':
        errors.companyName =
          validcompanyRegex.test(value)
            ? ''
            : 'Please enter valid company name';
        break;
      case 'name':
        errors.name =
          validNameRegex.test(value)
            ? ''
            : 'Full Name must be 3 characters long!';
        break;
      case 'email':
        errors.email =
          validEmailRegex.test(value)
            ? ''
            : 'Email is not valid!';
        break;
      case 'mobile':
        errors.mobile =
          validMobileRegex.test(value)
            ? ''
            : 'Mobile must be valid & 10 characters long!';
        break;
      case 'gst':
        errors.gst =
          validGstRegex.test(value)
            ? ''
            : 'Please enter valid GST Number';
        break;
      case 'businessType':
        errors.businessType =
          value.length < 1
            ? 'Please Select Bussiness Type'
            : '';
        break;
      case 'address':
        errors.address =
          value.length < 5
            ? 'Please Enter Address'
            : '';
        break;
      default:
        break;
    }

    this.setState({errors, [name]: value});
  }

  handleSubmit = (event) => {
    event.preventDefault();

    let userData = {
      'userType'      : this.state.userType,
      'name'          : this.state.name,
      'companyName'   : this.state.companyName,
      'mobile'        : this.state.mobile,
      'email'         : this.state.email,
      'gst'           : this.state.gst,
      'businessType'  : this.state.businessType,
      'address'       : this.state.address,
      'comments'      : this.state.comments,
    }

    if(this.state.isCaptchaverified){
      this.setState({
        captchamsg : ''
      })
      if(validateForm(userData)) {
        this.setState({
          formminavlid : ''
        })
        console.info('Valid Form');
        this._isMounted = true;
        {/*//this.props.newuser(userData);*/}
        userService.register(userData).then(response => {
          console.log('response======>', response);

          if(response && response.info && response.info.status && response.info.status == '201'){
              this.setState({
                name: '',
                email: '',
                mobile: '',
                companyName: '',
                userType: 'MERCHANT',
                gst : '',
                businessType : '',
                address : '',
                comments : '',
                errors: {
                  name: '',
                  email: '',
                  mobile: '',
                  companyName:'',
                  gst : '',
                  businessType : '',
                  address : '',
                }
              });
                this.toggleModal();
                console.log('response');
                this.open();
                this.cancelForm();
                this.resetRecaptcha();
          }else{
            console.log('api response here',response);

          }
        });
      }else{
        console.error('Invalid Form')
        this.setState({
          formminavlid : 'Please fill the form'
        })
      }
    }else {
      this.setState({
        captchamsg : 'Pleae tick the box to continue'
      })
    }


    console.log(this.state);


  }

  onRadioChange = (event) => {
    this.setState({'userType' : event.target.value})
  }

  cancelForm = () => {
    document.getElementById("join-us-form").reset();
  }

  componentWillUnmount() {
      this._isMounted = false;
    }


  // create a reset function
  resetRecaptcha = () => {
    this.captcha.reset();
  };

  render(){
    const {errors} = this.state;
    return(
      <div className="merchant_joinus_section " id="consumer_joinus">
        <div className="joinuspage__close">
            <img src="./../assets/close-x-black.png" alt="close" onClick={() => this.goback()}/>
        </div>
        <div className="merchant_joinus_form">
          <div className="joinus__form_header_title">
            <div className="joinus__fom_content">
              <h3>DOE</h3>
              <p>Rechargeable and Contactless Smart Card that Everyone can Use</p>
            </div>
          </div>
          <div className="joinus__services">
            <h3>Services</h3>
            <div className="joinus__fom_content_image">
              <img src="./../assets/merchant/merchant-joinus-image.png" />
            </div>
          </div>
          <div className="joinus_form_section">
            <div className="merchant_landing_joinus_container">
              <div className='wrapper joinus__page_container'>
                <div className='form-wrapper'>
                  <div className="joinus__sub-header">
                    <p>
                      Thank you for your interest.
                      Just fill in the details and our team will reach out with further steps.
                    </p>
                  </div>
                  <form id="join-us-form" onSubmit={this.handleSubmit} noValidate>
                    <div className="field_input_floating">
                      <div class="float_input_style">
                        <input type='text' className="input-box floating-input" name='companyName' placeholder="Company Name " onChange={this.handleChange}  noValidate />
                        <label for="fullname">Company Name*</label>
                      </div>
                      {errors.companyName.length > 0 &&
                        <span className='error_msg'>{errors.companyName}</span>}
                    </div>

                    <div className="field_input_floating">
                      <div class="float_input_style">
                        <input type='text' className="input-box floating-input" name='name' placeholder="Contact Person Name " onChange={this.handleChange}  noValidate />
                        <label for="fullname">Contact Person Name*</label>
                      </div>
                      {errors.name.length > 0 &&
                        <span className='error_msg'>{errors.name}</span>}
                    </div>

                    <div className="field_input_floating">
                      <div class="float_input_style">
                        <input type='email' className="input-box floating-input" name='email' placeholder="example@gmail.com " onChange={this.handleChange}  noValidate />
                          <label htmlFor="email">Email</label>
                      </div>
                      {errors.email.length > 0 &&
                        <span className='error_msg'>{errors.email}</span>}
                    </div>

                    <div className="field_input_floating">
                      <div class="float_input_style">
                        <input type='text' name='mobile' maxLength="10" className="input-box floating-input" placeholder="0111111111 " onChange={this.handleChange}  noValidate />
                          <label htmlFor="mobile">Mobile*</label>
                      </div>
                      {errors.mobile.length > 0 &&
                        <span className='error_msg'>{errors.mobile}</span>}
                    </div>

                    <div className="field_input_floating">
                      <div class="float_input_style">
                        <input type='text' name='gst' className="input-box floating-input" placeholder="11AAAA1111A1AA" onChange={this.handleChange}  noValidate />
                          <label htmlFor="gst">GST</label>
                      </div>
                      {errors.gst.length > 0 &&
                        <span className='error_msg'>{errors.gst}</span>}
                    </div>

                    <div className="field_input_floating">
                      <div class="float_input_style">
                        <input type='text' name='address' className="input-box floating-input" placeholder="Location " onChange={this.handleChange}  noValidate />
                          <label htmlFor="location">Location</label>
                      </div>
                      {errors.address.length > 0 &&
                        <span className='error_msg'>{errors.address}</span>}
                    </div>
                    <div className="field_input_floating">
                      <div class="float_input_style select_type">
                        <select name="businessType" onChange={this.handleChange}>
                          <option value=" ">Select Bussiness</option>
                          <option value="Toll">Toll</option>
                          <option value="Parking">Parking</option>
                          <option value="Retail">Retail</option>
                          <option value="Bus">Bus</option>
                          <option value="Metro">Metro</option>
                          <option value="Education">Education</option>
                          <option value="Railways">Railways</option>
                          <option value="Access">Access</option>
                          <option value="Loyality">Loyality</option>
                        </select>
                      </div>
                      {errors.businessType.length > 0 &&
                        <span className='error_msg'>{errors.businessType}</span>}
                    </div>

                    <div className="text_area_section">
                      <label>Say Something</label>
                      <textarea name="comments" onChange={this.handleChange}></textarea>
                    </div>

    								<div class="captcha__section">
                      <Recaptcha
                        sitekey="6LdD_MUUAAAAABq9Jqe4Mq3V18yuJ2ekq9CXcvdf"
                        ref={e => (this.captcha = e)}
                        render="explicit"
                        verifyCallback={this.verifyCallback}
                        onloadCallback={this.captchcallback}
                        type= 'image'
                      />
    								</div>

                    <p className='error_msg msg_center'>{this.state.captchamsg}</p>
                    <p className='error_msg msg_center'>{this.state.formminavlid}</p>
                    <div className='joinus_submit'>
                      <button>Submit</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

          <div className="merchant_joinus_doe_card_image">
            <img src="./../assets/merchant/merchant-mask-group.png" alt="doe" />
          </div>
        </div>
        <Modal show={this.state.isOpen}
          onClose={this.toggleModal}>
          <div class="success-checkmark">
            <div class="check-icon">
              <span class="icon-line line-tip"></span>
              <span class="icon-line line-long"></span>
              <div class="icon-circle"></div>
              <div class="icon-fix"></div>
            </div>
          </div>
          <h4>SUCCESSFUL SUBMITED</h4>
          <p>Your request has been placed. We will get back to you soon.</p>
        </Modal>

      </div>

    )
  }
}
export default MerchantJoinus;
