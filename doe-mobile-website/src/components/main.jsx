import React, { Component } from "react";
import {createBrowserHistory} from 'history';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import Consumer from './page_consumer.jsx';
import Aboutus from './page_aboutus.jsx'
import Joinus from './page_joinus.jsx'
import MerchantJoinus from './page_merchant_joinus.jsx'
import Partners from './page_partners.jsx'
import TermsnCondition from './page_terms.jsx'
import Privacypolicy from './page_privacy.jsx'
import Contactus from './page_contactus.jsx';
import MerchantLanding from './page_landing_merchant.jsx';
import ConsumerFindOutMore from './page_consumer_findout_more.jsx';
import CorporateLanding from './page_landing_corporate.jsx';

import staticContent from "../json/static-content.json";
import devices from "../json/devices.json";

import MerchantToll from "./merchant-services/toll/page_toll.jsx";
import MerchantParking from "./merchant-services/parking/page_parking.jsx";
import MerchantRetail from "./merchant-services/retail/page_retail.jsx";
import MerchantBus from "./merchant-services/bus/page_bus.jsx";
import MerchantMetro from "./merchant-services/metro/page_metro.jsx";
import MerchantRailway from "./merchant-services/railway/page_railway.jsx";
import MerchantEducation from "./merchant-services/education/page_education.jsx";
import FeatureDetails from "./merchant-services/toll/page_feature_details.jsx";
import ParkingFeatureDetails from "./merchant-services/parking/page_feature_details.jsx";
import RetailFeatureDetails from "./merchant-services/retail/page_feature_details.jsx";
import BusFeatureDetails from "./merchant-services/bus/page_feature_details.jsx";
import MetroFeatureDetails from "./merchant-services/metro/page_feature_details.jsx";
import RailwayFeatureDetails from "./merchant-services/railway/page_feature_details.jsx";
import EducationFeatureDetails from "./merchant-services/education/page_feature_details.jsx";
import ProductDetails from "./merchant-services/page_product_details.jsx";
import ScrollIntoView from "./scrollIntoView.jsx";

import MerchantAccess from "./merchant-services/access/page_access.jsx";
import AccessServiceDetails from "./merchant-services/access/page_service_details.jsx";

import MerchantLoyalty from "./merchant-services/loyalty/page_loyalty.jsx";
import LoyaltyServiceDetails from "./merchant-services/loyalty/page_service_details.jsx";

import ConsumerServiceDetails from "./page_consumer_services_menu.jsx";
import ConsumerFeatureDetails from "./page_consumer_features_menu.jsx";
import MerchantFeatureDetails from "./page_merchant_features_menu.jsx";
import PageNotFound from "./page_404.jsx";


class Main extends Component {
  constructor(props){
    super(props);
    this.state = {
      staticContent : staticContent,
      devices: devices,
    }
    console.log(staticContent);
    console.log(devices);
  }
  render(){
    return (
      <main>
        <Router>
          <ScrollIntoView>
            <Switch>
                <Route exact path="/">
                  <Redirect to="/home" />
                </Route>
                <Route exact path='/home' render={(props) => <Consumer {...props} content={this.state.staticContent} />}/>
                <Route exact path='/corporate' render={(props) => <CorporateLanding {...props} content={this.state.staticContent}/>}/>
                <Route exact path='/merchant' render={(props) => <MerchantLanding {...props} content={this.state.staticContent}/>}/>

                <Route exact path='/home/feature' render={(props) => <ConsumerFeatureDetails {...props} content={this.state.staticContent} />}/>
                <Route exact path='/home/service' render={(props) => <ConsumerServiceDetails {...props} content={this.state.staticContent} />}/>
                <Route exact path='/merchant/feature' render={(props) => <MerchantFeatureDetails {...props} content={this.state.staticContent}/>}/>
                <Route exact path='/merchant/joinus' render={(props) => <MerchantJoinus {...props} content={this.state.staticContent}/>}/>

                <Route exact path='/aboutus' render={() => <Aboutus device={this.state.devices} content={this.state.staticContent} />}/>
                <Route exact path='/contactus' render={() => <Contactus content={this.state.staticContent} />}/>
                <Route exact path='/joinus' render={(props) => <Joinus {...props} />}/>
                <Route exact path='/partners' render={() => <Partners content={this.state.staticContent}/>}/>
                <Route exact path='/terms' render={() => <TermsnCondition content={this.state.staticContent} />}/>
                <Route exact path='/privacy-policy' render={() => <Privacypolicy content={this.state.staticContent} />}/>
                <Route exact path='/find-more' render={(props) => <ConsumerFindOutMore {...props} content={this.state.staticContent}/>}/>

                <Route exact path='/toll' render={(props) => <MerchantToll {...props} device={this.state.devices} content={this.state.staticContent}/>}/>
                <Route exact path='/toll/feature-info' render={(props) => <FeatureDetails {...props} device={this.state.devices} content={this.state.staticContent}/>}/>

                <Route exact path='/parking' render={(props) => <MerchantParking {...props} device={this.state.devices} content={this.state.staticContent}/>}/>
                <Route exact path='/parking/feature-info' render={(props) => <ParkingFeatureDetails {...props} device={this.state.devices} content={this.state.staticContent}/>}/>

                <Route exact path='/retail' render={(props) => <MerchantRetail {...props} device={this.state.devices} content={this.state.staticContent}/>}/>
                <Route exact path='/retail/feature-info' render={(props) => <RetailFeatureDetails {...props} device={this.state.devices} content={this.state.staticContent}/>}/>

                <Route exact path='/access' render={(props) => <MerchantAccess {...props} device={this.state.devices} content={this.state.staticContent}/>}/>
                <Route exact path='/access/service-info' render={(props) => <AccessServiceDetails {...props} device={this.state.devices} content={this.state.staticContent}/>}/>

                <Route exact path='/loyalty' render={(props) => <MerchantLoyalty {...props} device={this.state.devices} content={this.state.staticContent}/>}/>
                <Route exact path='/loyalty/service-info' render={(props) => <LoyaltyServiceDetails {...props} device={this.state.devices} content={this.state.staticContent}/>}/>

                <Route exact path='/bus' render={(props) => <MerchantBus {...props} device={this.state.devices} content={this.state.staticContent}/>}/>
                <Route exact path='/bus/feature-info' render={(props) => <BusFeatureDetails {...props} device={this.state.devices} content={this.state.staticContent}/>}/>

                <Route exact path='/metro' render={(props) => <MerchantMetro {...props} device={this.state.devices} content={this.state.staticContent}/>}/>
                <Route exact path='/metro/feature-info' render={(props) => <MetroFeatureDetails {...props} device={this.state.devices} content={this.state.staticContent}/>}/>

                <Route exact path='/railway' render={(props) => <MerchantRailway {...props} device={this.state.devices} content={this.state.staticContent}/>}/>
                <Route exact path='/railway/feature-info' render={(props) => <RailwayFeatureDetails {...props} device={this.state.devices} content={this.state.staticContent}/>}/>

                <Route exact path='/education' render={(props) => <MerchantEducation {...props} device={this.state.devices} content={this.state.staticContent}/>}/>
                <Route exact path='/education/feature-info' render={(props) => <EducationFeatureDetails {...props} device={this.state.devices} content={this.state.staticContent}/>}/>

                <Route exact path='/product-info' render={(props) => <ProductDetails {...props} device={this.state.devices} content={this.state.staticContent}/>}/>

                <Route render={() => <PageNotFound />} />
              </Switch>
          </ScrollIntoView>
        </Router>
      </main>
    );
  }
}

export default Main;
