import React, { Component } from 'react';
import { Navbar, Nav, NavDropdown, Form, FormControl, Button } from "react-bootstrap";
import { Link } from 'react-router-dom';
import Carousel from 'react-bootstrap/Carousel'

class MerchantLoyalty extends Component {
  constructor(){
     super();
  }

  render(){
    return(
      <div className="Merchant_sub-section" id="merchant_loyalty">
        <div className="section__ribbon">
          <p>Loyalty</p>
        </div>
        <div className="service-merchant-image">
          <img src="./../assets/merchant/merchantloyalty-logo.png" />
        </div>
        <div className="services__circle_layout mer_services__circle_layout">
          <div className="service__circle_view loylity__circle_view mer_service__circle_view">
            <img className="" src="./../assets/services/group-airlines.png" alt=""/>
          </div>
          <div className="service__circle_view loylity__circle_view mer_service__circle_view">
            <img className="" src="./../assets/services/group-clubs.png" alt=""/>
          </div>
          <div className="service__circle_view loylity__circle_view mer_service__circle_view">
            <img className="" src="./../assets/services/group-restarunt.png" alt=""/>
          </div>
          <div className="service__circle_view loylity__circle_view mer_service__circle_view">
            <img className="" src="./../assets/services/group-petro.png" alt=""/>
          </div>
        </div>
        <div className="more-details-button">
          <Link to="/loyalty">
            <button className="mer_find_out">FIND OUT MORE</button>
          </Link>
        </div>
      </div>
    )
  }
}
export default MerchantLoyalty;
