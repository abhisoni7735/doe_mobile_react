import React, { Component } from 'react';
import { Navbar, Nav, NavDropdown, Form, FormControl, Button } from "react-bootstrap";
import doeLogo from "../assets/logo.png";
import { Link } from 'react-router-dom';
import Common_header from "./common_header";
import AboutDoeConsumer from "./about_doe_consumer";
import ConsumerFeatures from "./page_consumer_feature";
import ConsumerServices from "./page_consumer_services";
import ConsumerJoinus from "./page_consumer_joinus";
import Consumervideos from "./page_consumer_videos";
import ConsumerScope from "./page_consumer_scope";
import ConsumerFAQ from "./page_consumer_faq";

class Consumer extends Component {
  constructor(props){
    super(props);
  }

  render(){
    return(
      <div className="Consumer_main">
        <Common_header location={this.props.location}/>
        <AboutDoeConsumer content={this.props.content} />
        <ConsumerFeatures content={this.props.content} />
        <ConsumerScope />
        <ConsumerServices content={this.props.content} />
        <ConsumerJoinus />
        <Consumervideos content={this.props.content}/>
        <ConsumerFAQ content={this.props.content} />
      </div>
    )
  }
}


export default Consumer;
