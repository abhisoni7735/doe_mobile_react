import React, { Component } from 'react';
import { Navbar, Nav, NavDropdown, Form, FormControl, Button } from "react-bootstrap";
import { Link } from 'react-router-dom';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
import {
  Menu,
  MenuList,
  MenuButton,
  MenuItem,
  MenuItems,
  MenuPopover,
  MenuLink,
} from "@reach/menu-button";
import "@reach/menu-button/styles.css";
import queryString from 'query-string';


class ConsumerFeatureDetails extends Component {
  render(){
    let query = queryString.parse(this.props.location.search);
    console.log(query);
    console.log(this.props.content);
    return(
      <div className="merchant_feature_info_page">
          <div className="close__mark">

            <Link to="/home#consumer_features"><img src="./../assets/close-x.png" alt="close" /></Link>
          </div>
        <div className="service__feature_image">
          <img src="./../assets/corner-image.png" alt=""/>
        </div>
        <div className="feature_info_contnet">
          <h4 className="feature_section_title" dangerouslySetInnerHTML={{ __html: this.props.content.LANDING_PAGE.FEATURES.CONTENT_TABS[query.name].NAME }}></h4>
          <p dangerouslySetInnerHTML={{ __html: this.props.content.LANDING_PAGE.FEATURES.CONTENT_TABS[query.name].DESCRIPTION_PART1 }}></p>
        </div>
        <div className="feature_divide_line">
          <img src="./../assets/common_divide_line.png" alt="divide" />
        </div>

        <div className="feature_info_contnet">
          <p dangerouslySetInnerHTML={{ __html: this.props.content.LANDING_PAGE.FEATURES.CONTENT_TABS[query.name].DESCRIPTION_PART2 }}></p>
        </div>

      </div>
    )
  }
}
export default ConsumerFeatureDetails;
