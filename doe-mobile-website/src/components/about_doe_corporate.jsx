import React, { Component } from 'react';
import { Navbar, Nav, NavDropdown, Form, FormControl, Button } from "react-bootstrap";
import { Link } from 'react-router-dom';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';

class AboutDoeCorporate extends Component {
  constructor(){
     super();
  }

  Clickshowdiv () {

  }
  render(){
    return(
      <div className="Corporate_main-section" id="corporate_home">
        <div className="corporate-sub-content">
          <div className="image--content">
            <img src="./../assets/corporate/about_corporate.png" />
          </div>
          <div className="first-content">
          <p>
            Providing a Seamless <strong>Payment, Access and Loyalty</strong> service for India
          </p>
          </div>
          <div className="second-content">
            <span>Know more about our Team and Partners.</span>
          </div>
        </div>
      </div>
    )
  }
}
export default AboutDoeCorporate;
