import React, { Component } from "react";
import { hot } from "react-hot-loader";
import ScrollableSection, { ScrollableLink } from 'react-update-url-on-scroll';
import Common_header from "./common_header.jsx";
import { Link } from 'react-router-dom';
import AboutDoeCorporate from "./about_doe_corporate";
import CorporateJoinus from "./page_corporate_joinus";
import CorporateVideos from "./page_corporate_videos";

class CorporateLanding extends Component {
  constructor(props){
    super(props);
    this.state = {
      showndiv: false,
      currentPage: null
    }
  }

  Clickshowdiv () {
    this.setState({
      showndiv : true,
    })
  }
  render() {
    const content = this.props.content;
    return (

      <div className="Merchant_main">
        <Common_header location={this.props.location}/>
        <AboutDoeCorporate content={this.props.content}/>
        <CorporateJoinus content={this.props.content}/>
        <CorporateVideos content={this.props.content} />
      </div>
    );
  }
}

export default CorporateLanding;
