import React, { Component } from 'react';
import { Navbar, Nav, NavDropdown, Form, FormControl, Button } from "react-bootstrap";
import { Link } from 'react-router-dom';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
import VideoModal from './video_modal';
import YouTube from 'react-youtube';

class Consumervideos extends Component {

  constructor(props){
    super(props);
    this.state = {
      isOpen: false,
      videopopupSrc:'https://www.youtube.com/embed/1g4lJQlI_PA'

    }
  }

  toggleModal = () => {
    console.log('this is trigerd for popu[]')
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  close = () => {
    this.setState({ isOpen: false });
  }

  open = (video) => {

    this.setState({ videopopupSrc: video.videoid });
    this.setState({ isOpen: true });
  }
  _onReady(event) {
   // access to player in all event handlers via event.target
   event.target.pauseVideo();
 }

  render(){
    const content = this.props.content; 
    const opts = {

      width: '100%',
      playerVars: {
        // https://developers.google.com/youtube/player_parameters
        autoplay: 1,
      },
    };

    return(
      <div className="Consumer_video_section consumer__section" id="consumer_videos">
        <div className="section__ribbon">
          <p>Videos</p>
        </div>
        <div className="video__section_layout">
          <div className="videosection_cards_layout">
          {
            content.LANDING_PAGE.VIDEOS.map( (video, i )=> <div key={i} className="videosection_card">
              <div className="video_thumnail_image" onClick={()=> this.open(video)} >
                <img alt="" className="bgcover_image" src={window.location.origin + video.thumnail} />
                <img alt="" className="play_icon"  src={window.location.origin + video.playbutton}  />
              </div>
              <div className="video_content">
                <p className="Product_title">{video.content}</p>
                <p className="product_description">{video.description}</p>
              </div>
            </div> )
          }
          </div>
        </div>
        <VideoModal show={this.state.isOpen}
          onClose={this.toggleModal}>
          <YouTube videoId={this.state.videopopupSrc} opts={opts} onReady={this._onReady} />
        </VideoModal>


      </div>
    )
  }
}
export default Consumervideos;
