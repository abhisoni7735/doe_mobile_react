import React, { Component } from "react";
import Common_header from "./common_header";
import { Link } from 'react-router-dom';


class Privacypolicy extends Component {

    render(){
      const content = this.props.content;
      return(
            <div className="privacy_terms__page">
                <Common_header/>
                <div className="privacy_terms_title">
                    <img src="./../assets/privacy.png" />
                    <h2>Privacy Policy</h2>
                </div>
                <div className="privacy_terms_content">

                  {content.PRIVACY_POLICY.map((details, j) => {
                   return<div key={j}>
                    <h4 dangerouslySetInnerHTML={{ __html: details.heading }}></h4>
                    <p dangerouslySetInnerHTML={{ __html: details.summary }}></p>
                    <p dangerouslySetInnerHTML={{ __html: details.points }}></p>
                  </div>
                  })}
                  <h4>How do we use your information?</h4>
                  <br/>
                  <p>For information about how to contact DOE CARDS SOLUTIONS PVT. LTD, please visit our Join us page.</p>
                  <br/>
                  <p>Contacting Us <br/> If there are any questions regarding this privacy policy, you may contact us using the information below.</p>
                  <address>
                    <a href="http://www.doecards.com/">http://www.doecards.com/</a><br/>
                    #14, 17th Cross Road, Sector 7, <br/>
                    HSR Layout, Benguluru, Karnataka 560102<br/>
                    Phone: +91 000 000 0000<br/>
                    <a href="mailto:contact@doecards.com">Email: contact@doecards.com</a>
                  </address>
                </div>
            </div>
          )}
}

export default Privacypolicy;
