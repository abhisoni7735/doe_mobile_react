import React, { Component } from "react";
import Common_header from "./common_header";
import { connect } from 'react-redux';
import Carousel from 'react-bootstrap/Carousel';

class Partners extends Component {

  constructor(){
    super();
    this.state = {
      "selectedSection" : "PARTNERS"
    }
  }

  changeSection(section){
    this.setState({
      selectedSection: section
    });
  }

  render(){
    const content = this.props.content;
    console.log(content);
    return(
          <div className="partners__page">
              <Common_header/>
            <div className="page_layout__container">
              <div className="partners_bgimage">
                <img src="./../assets/partners_bg.png" alt="partners_bg"/>
              </div>
              <div className="partners__content">
                <div className={"content_tab " + (this.state.selectedSection == "PARTNERS" ? "active" : "")} onClick={() => this.changeSection('PARTNERS')}>
                  Our Partners <span>|</span>
                </div>
                <div className={"content_tab " + (this.state.selectedSection == "MEMBERS" ? "active" : "")} onClick={() => this.changeSection('MEMBERS')}>
                  Our Members <span>|</span>
                </div>
                <div className={"content_tab " + (this.state.selectedSection == "CUSTOMERS" ? "active" : "")} onClick={() => this.changeSection('CUSTOMERS')}>
                  Our Customers
                </div>

              </div>
            </div>
            {this.state.selectedSection == "PARTNERS" ? <div className="our_partners">
              {
                content.PARTNERS.PARTNERS.map( (partner, i) =>  <div key={i} className="our_partners_bio">
                  <div className="partners__image">
                        <img  src={window.location.origin + partner.IMAGE} />
                  </div>
                  <h3 className="our_partners_name" dangerouslySetInnerHTML={{ __html: partner.NAME }}></h3>
                  <h4 className="our_partners_subtitle" dangerouslySetInnerHTML={{ __html: partner.SUMMARY }}></h4>
                  <p className="address_partners" dangerouslySetInnerHTML={{ __html: partner.COMPANY_ADDRESS }}></p>
                    <p className="about_partners" dangerouslySetInnerHTML={{ __html: partner.DESCRIPTION }}></p>
                </div>)
              }
            </div> : null}

            {(this.state.selectedSection == "MEMBERS" || this.state.selectedSection == "CUSTOMERS") ? <div className="our_partners">
              <div className="our_partners_bio">
                <div className="partners__image">
                      <img  src="./../assets/coming-soon.png" />
                </div>
              </div>
            </div> : null}

          </div>


    )
  }
}


export default Partners;
