import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Navbar, Nav, NavDropdown, Form, FormControl, Button, NavItem } from "react-bootstrap";
import { Link } from 'react-router-dom';

import Accordion from 'react-bootstrap/Accordion';
import Card from 'react-bootstrap/Card'



class ConsumerFAQ extends Component {
  constructor(){
     super();
  }


  render(){
    const content = this.props.content;
    return(
      <div className="Consumer_faq_section consumer__section" id="consumer_faqs">
        <div className="section__ribbon">
          <p>FAQ</p>
        </div>
        <div className="FAQ__section_layout">
          <h4>If you’ve got any questions about how we can make your life easier,please check out our FAQ.</h4>

          <div className="faqsection__consumer_layout">
            <Accordion defaultActiveKey="0">
            {
              content.LANDING_PAGE.FAQS[0].QNA.map( (qna, i) =>  <Card key={i}>
                <Accordion.Toggle as={Card.Header} eventKey={i} dangerouslySetInnerHTML={{ __html: qna.question }}></Accordion.Toggle>
                <Accordion.Collapse eventKey={i}>
                  <Card.Body dangerouslySetInnerHTML={{ __html: qna.answer }}></Card.Body>
                </Accordion.Collapse>
              </Card>)
            }
            </Accordion>

          </div>
        </div>

      </div>
    )
  }
}
export default ConsumerFAQ;
