import React, { Component } from 'react';
import { Navbar, Nav, NavDropdown, Form, FormControl, Button } from "react-bootstrap";
import { Link } from 'react-router-dom';
import Carousel from 'react-bootstrap/Carousel'

class MerchantPayment extends Component {
  constructor(){
     super();
  }

  render(){
    return(
      <div className="Merchant_sub-section" id="merchant_payment">
        <div className="section__ribbon">
          <p>Payments</p>
        </div>
        <div className="service-merchant-image">
          <img src="./../assets/services/merchant-pay-logo.png" />
        </div>
        <div className="services__circle_layout">
          <Link to="/toll">
            <div className="service__circle_view toll_circle_view mer_payment_cir">
              <img className="service_non-hover" src="./../assets/services/toll-merchant-service.png" alt="" />
              <img className="service_hover" src="./../assets/services/toll-merchant-service-hover.png" alt="" />
            </div>
          </Link>

          <Link to="/bus">
            <div className="service__circle_view bus_circle_view mer_payment_cir">
              <img className="service_non-hover" src="./../assets/services/bus-merchant-service.png" alt="" />
              <img className="service_hover" src="./../assets/services/bus-merchant-service-hover.png" alt="" />
            </div>
          </Link>

          <Link to="/parking">
            <div className="service__circle_view parking_circle_view mer_payment_cir">
              <img className="service_non-hover" src="./../assets/services/parking-merchant-service.png" alt="" />
              <img className="service_hover" src="./../assets/services/parking-merchant-service-hover.png" alt="" />
            </div>
          </Link>
          <Link to="/metro">
            <div className="service__circle_view metro_circle_view mer_payment_cir">
              <img className="service_non-hover" src="./../assets/services/metro-merchant-service.png" alt="" />
              <img className="service_hover" src="./../assets/services/metro-merchant-service-hover.png" alt="" />
            </div>
          </Link>
          <Link to="/railway">
          <div className="service__circle_view railways_circle_view mer_payment_cir">
            <img className="service_non-hover" src="./../assets/services/railway-merchant-service.png" alt="" />
            <img className="service_hover" src="./../assets/services/railway-merchant-service-hover.png" alt="" />
          </div>
          </Link>

          <Link to="/education">
            <div className="service__circle_view education_circle_view mer_payment_cir">
              <img className="service_non-hover" src="./../assets/services/edu-merchant-service.png" alt="" />
              <img className="service_hover" src="./../assets/services/edu-merchant-service-hover.png" alt="" />
            </div>
          </Link>

          <Link to="/retail">
            <div className="service__circle_view retail_circle_view mer_payment_cir">
              <img className="service_non-hover" src="./../assets/services/retail-merchant-service.png" alt="" />
              <img className="service_hover" src="./../assets/services/retail-merchant-service-hover.png" alt="" />
            </div>
          </Link>

        </div>
      </div>
    )
  }
}
export default MerchantPayment;
