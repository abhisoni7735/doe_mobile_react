import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from 'react-router-dom';
import { hot, setConfig } from "react-hot-loader";

setConfig({
    showReactDomPatchNotification: false
})

import App from "./App.jsx";

import 'bootstrap/dist/css/bootstrap.min.css';



ReactDOM.render(<BrowserRouter>
    <App />
  </BrowserRouter>, document.getElementById("root"));
