import React, { Component } from "react";
import { Router, Switch, Route, Link } from "react-router-dom";
import { hot } from "react-hot-loader";

import {Provider} from 'react-redux';

import Main from './components/main.jsx';
import store from './store.js';
import "./scss/App.scss";

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="App">
          <Main/>
        </div>
      </Provider>

    );
  }
}

export default hot(module)(App);
