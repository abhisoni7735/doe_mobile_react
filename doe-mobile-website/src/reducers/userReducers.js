// any actions we can perform here
import { NEW_USER } from '../actions/types.js';

const initialState = {
  //items : [], //post that coming from action fertch req
  user:{}, //single post add
}

export default function (state = initialState, action) {
  switch(action.type) {
    case NEW_USER:
    return {
      ...state,
      user:action.payload
    }
    default:
     return state;

  }
}
