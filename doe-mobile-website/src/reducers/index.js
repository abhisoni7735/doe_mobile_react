import {combineReducers} from 'redux';
import userReducer from './userReducers.js';

export default combineReducers({
  users: userReducer
})
